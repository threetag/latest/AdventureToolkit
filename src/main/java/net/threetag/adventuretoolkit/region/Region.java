package net.threetag.adventuretoolkit.region;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.threetag.adventuretoolkit.region.feature.IRegionFeature;
import net.threetag.adventuretoolkit.region.feature.IRegionFeatureSerializer;
import net.threetag.adventuretoolkit.region.feature.RegionFeatures;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Region {

    private final ITextComponent name;
    private final RegistryKey<World> dimension;
    private final List<AxisAlignedBB> boxes;
    private final List<IRegionFeature> features = new ArrayList<>();
    private final ResourceLocation parent;
    private final List<Region> children = new ArrayList<>();

    public Region(ITextComponent name, RegistryKey<World> dimension, List<AxisAlignedBB> boxes, ResourceLocation parent) {
        this.name = name;
        this.dimension = dimension;
        this.boxes = boxes;
        this.parent = parent;
    }

    public Region(ITextComponent name, RegistryKey<World> dimension, AxisAlignedBB box) {
        this(name, dimension, Collections.singletonList(box), null);
    }

    public ITextComponent getName() {
        return this.name;
    }

    public void onEnter(PlayerEntity playerEntity) {
        for (IRegionFeature feature : this.features) {
            feature.onEnter(playerEntity);
        }
    }

    public void onLeave(PlayerEntity playerEntity) {
        for (IRegionFeature feature : this.features) {
            feature.onLeave(playerEntity);
        }
    }

    public Region addFeature(IRegionFeature feature) {
        feature.setRegion(this);
        this.features.add(feature);
        return this;
    }

    public Region addChild(Region region) {
        this.children.add(region);
        return this;
    }

    public List<IRegionFeature> getFeatures() {
        return ImmutableList.copyOf(features);
    }

    public List<IRegionFeature> getFeaturesByType(IRegionFeatureSerializer serializer) {
        return this.features.stream().filter(feature -> feature.getSerializer() == serializer).collect(Collectors.toList());
    }

    @Nullable
    public ResourceLocation getParent() {
        return this.parent;
    }

    public Region isInRegion(Entity entity) {
        return this.isInRegion(entity.level, entity.position());
    }

    public Region isInRegion(World world, Vector3d pos) {
        if (!world.dimension().location().equals(this.dimension.location())) {
            return null;
        }

        for (Region child : this.children) {
            Region region = child.isInRegion(world, pos);
            if (region != null) {
                return region;
            }
        }

        for (AxisAlignedBB box : this.boxes) {
            if (box.contains(pos)) {
                return this;
            }
        }
        return null;
    }

    public void write(PacketBuffer buffer) {
        buffer.writeComponent(this.name);
        buffer.writeResourceLocation(this.dimension.location());
        buffer.writeBoolean(this.parent != null);

        if (this.parent != null) {
            buffer.writeResourceLocation(this.parent);
        }

        buffer.writeInt(this.boxes.size());
        for (AxisAlignedBB box : this.boxes) {
            buffer.writeDouble(box.minX);
            buffer.writeDouble(box.minY);
            buffer.writeDouble(box.minZ);
            buffer.writeDouble(box.maxX);
            buffer.writeDouble(box.maxY);
            buffer.writeDouble(box.maxZ);
        }
        buffer.writeInt(this.features.stream().filter(IRegionFeature::needsSyncing).collect(Collectors.toList()).size());
        for (IRegionFeature feature : this.features) {
            if (feature.needsSyncing()) {
                buffer.writeResourceLocation(feature.getSerializer().getRegistryName());
                feature.getSerializer().write(buffer, feature);
            }
        }
    }

    public static Region read(PacketBuffer buffer) {
        ITextComponent name = buffer.readComponent();
        RegistryKey<World> dimension = RegistryKey.create(Registry.DIMENSION_REGISTRY, buffer.readResourceLocation());
        ResourceLocation parent = buffer.readBoolean() ? buffer.readResourceLocation() : null;
        int size = buffer.readInt();
        List<AxisAlignedBB> boxes = Lists.newArrayList();
        for (int i = 0; i < size; i++) {
            boxes.add(new AxisAlignedBB(buffer.readDouble(), buffer.readDouble(), buffer.readDouble(), buffer.readDouble(), buffer.readDouble(), buffer.readDouble()));
        }
        Region region = new Region(name, dimension, boxes, parent);
        size = buffer.readInt();
        for (int i = 0; i < size; i++) {
            ResourceLocation id = buffer.readResourceLocation();
            region.addFeature(RegionFeatures.REGISTRY.get().getValue(id).read(buffer));
        }
        return region;
    }

}
