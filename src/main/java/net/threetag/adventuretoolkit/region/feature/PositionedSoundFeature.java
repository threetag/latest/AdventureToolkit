package net.threetag.adventuretoolkit.region.feature;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.threetag.adventuretoolkit.client.audio.PositionedRegionSound;
import net.threetag.threecore.util.documentation.IDocumentationSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PositionedSoundFeature extends AbstractRegionFeature {

    private final Vector3d position;
    private final ResourceLocation sound;
    private final float volume, pitch;
    private final boolean repeat;
    private final int repeatDelay;

    public PositionedSoundFeature(Vector3d position, ResourceLocation sound, float volume, float pitch, boolean repeat, int repeatDelay) {
        this.position = position;
        this.sound = sound;
        this.volume = volume;
        this.pitch = pitch;
        this.repeat = repeat;
        this.repeatDelay = repeatDelay;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void onEnter(PlayerEntity player) {
        Minecraft.getInstance().getSoundManager().play(new PositionedRegionSound(this.region, this.sound, this.volume, this.pitch, this.repeat, this.repeatDelay, this.position.x, this.position.y, this.position.z));
    }

    @Override
    public IRegionFeatureSerializer getSerializer() {
        return RegionFeatures.POSITIONED_SOUND.get();
    }

    public static class Serializer extends ForgeRegistryEntry<IRegionFeatureSerializer> implements IRegionFeatureSerializer, IDocumentationSettings {

        @Override
        public IRegionFeature read(JsonObject json) {
            Vector3d pos = new Vector3d(JSONUtils.getAsFloat(json, "pos_x"), JSONUtils.getAsFloat(json, "pos_y"), JSONUtils.getAsFloat(json, "pos_z"));
            ResourceLocation sound = new ResourceLocation(JSONUtils.getAsString(json, "sound"));
            float volume = JSONUtils.getAsFloat(json, "volume", 1F);
            float pitch = JSONUtils.getAsFloat(json, "pitch", 1F);
            boolean repeat = JSONUtils.getAsBoolean(json, "repeat", true);
            int repeatDelay = JSONUtils.getAsInt(json, "repeat_delay", 0);
            return new PositionedSoundFeature(pos, sound, volume, pitch, repeat, repeatDelay);
        }

        @Override
        public IRegionFeature read(PacketBuffer buffer) {
            Vector3d pos = new Vector3d(buffer.readDouble(), buffer.readDouble(), buffer.readDouble());
            ResourceLocation sound = buffer.readResourceLocation();
            float volume = buffer.readFloat();
            float pitch = buffer.readFloat();
            boolean repeat = buffer.readBoolean();
            int repeatDelay = buffer.readInt();
            return new PositionedSoundFeature(pos, sound, volume, pitch, repeat, repeatDelay);
        }

        @Override
        public void write(PacketBuffer buffer, IRegionFeature feature) {
            PositionedSoundFeature positionedSoundFeature = (PositionedSoundFeature) feature;
            buffer.writeDouble(positionedSoundFeature.position.x);
            buffer.writeDouble(positionedSoundFeature.position.y);
            buffer.writeDouble(positionedSoundFeature.position.z);
            buffer.writeResourceLocation(positionedSoundFeature.sound);
            buffer.writeFloat(positionedSoundFeature.volume);
            buffer.writeFloat(positionedSoundFeature.pitch);
            buffer.writeBoolean(positionedSoundFeature.repeat);
            buffer.writeInt(positionedSoundFeature.repeatDelay);
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public ResourceLocation getId() {
            return this.getRegistryName();
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public List<String> getColumns() {
            return Arrays.asList("Setting", "Type", "Description", "Required", "Fallback Value");
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public List<Iterable<?>> getRows() {
            List<Iterable<?>> rows = new ArrayList<>();
            rows.add(Arrays.asList("pos_x", Float.class, "The x-position where the sound will be displayed", true, null));
            rows.add(Arrays.asList("pos_y", Float.class, "The y-position where the sound will be displayed", true, null));
            rows.add(Arrays.asList("pos_z", Float.class, "The z-position where the sound will be displayed", true, null));
            rows.add(Arrays.asList("sound", ResourceLocation.class, "The ID for the sound", true, null));
            rows.add(Arrays.asList("volume", Float.class, "Volume of the sound", false, 1F));
            rows.add(Arrays.asList("pitch", Float.class, "Pitch of the sound", false, 1F));
            rows.add(Arrays.asList("repeat", Boolean.class, "If true, the sound will play on an infinite loop", false, true));
            rows.add(Arrays.asList("repeat_delay", Integer.class, "If the sound runs on a loop, this determines the amount of ticks for the breaks inbetween the repeats", false, 0));
            return rows;
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public JsonElement getExampleJson() {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("type", this.getId().toString());
            jsonObject.addProperty("pos_x", -69);
            jsonObject.addProperty("pos_y", 42);
            jsonObject.addProperty("pos_z", 111);
            jsonObject.addProperty("sound", "minecraft:ambient.cave");
            jsonObject.addProperty("volume", 1F);
            jsonObject.addProperty("pitch", 1F);
            jsonObject.addProperty("repeat", true);
            jsonObject.addProperty("repeat_delay", 20);
            return jsonObject;
        }
    }

}
