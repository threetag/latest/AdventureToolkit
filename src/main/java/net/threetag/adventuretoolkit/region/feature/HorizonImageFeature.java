package net.threetag.adventuretoolkit.region.feature;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Direction;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.threetag.threecore.util.documentation.IDocumentationSettings;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HorizonImageFeature extends AbstractRegionFeature implements ISkyRenderFeature {

    private final ResourceLocation texture;
    private final float alpha;
    private final List<Direction> directions;

    public HorizonImageFeature(ResourceLocation texture, float alpha, List<Direction> directions) {
        this.texture = texture;
        this.alpha = alpha;
        this.directions = directions;
    }

    @Override
    public IRegionFeatureSerializer getSerializer() {
        return RegionFeatures.HORIZON_IMAGE.get();
    }

    @Override
    public ResourceLocation getTexture() {
        return texture;
    }

    @Override
    public float getAlpha() {
        return alpha;
    }

    @Override
    public List<Direction> getDirections() {
        return directions;
    }

    public static class Serializer extends ForgeRegistryEntry<IRegionFeatureSerializer> implements IRegionFeatureSerializer, IDocumentationSettings {

        @Override
        public IRegionFeature read(JsonObject json) {
            List<Direction> directions = new ArrayList<>();
            if (JSONUtils.isValidNode(json, "directions")) {
                JsonArray jsonArray = JSONUtils.getAsJsonArray(json, "directions");
                for (int i = 0; i < jsonArray.size(); i++) {
                    Direction direction = Direction.byName(jsonArray.get(i).getAsString());

                    if (direction != null && !directions.contains(direction)) {
                        directions.add(direction);
                    }
                }
            }
            return new HorizonImageFeature(new ResourceLocation(JSONUtils.getAsString(json, "texture")), JSONUtils.getAsFloat(json, "alpha", 1F), directions);
        }

        @Nullable
        @Override
        public IRegionFeature read(PacketBuffer buffer) {
            List<Direction> directions = new ArrayList<>();
            int amount = buffer.readInt();
            for (int i = 0; i < amount; i++) {
                Direction direction = Direction.from3DDataValue(buffer.readInt());

                if (!directions.contains(direction)) {
                    directions.add(direction);
                }
            }

            return new HorizonImageFeature(buffer.readResourceLocation(), buffer.readFloat(), directions);
        }

        @Override
        public void write(PacketBuffer buffer, IRegionFeature feature) {
            HorizonImageFeature horizonImageFeature = (HorizonImageFeature) feature;
            buffer.writeInt(horizonImageFeature.directions.size());
            for (int i = 0; i < horizonImageFeature.directions.size(); i++) {
                buffer.writeInt(horizonImageFeature.directions.get(i).get3DDataValue());
            }
            buffer.writeResourceLocation(horizonImageFeature.texture);
            buffer.writeFloat(horizonImageFeature.alpha);
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public ResourceLocation getId() {
            return this.getRegistryName();
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public List<String> getColumns() {
            return Arrays.asList("Setting", "Type", "Description", "Required", "Fallback Value");
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public List<Iterable<?>> getRows() {
            List<Iterable<?>> rows = new ArrayList<>();
            rows.add(Arrays.asList("texture", ResourceLocation.class, "The path to the texture for the skybox", true, null));
            rows.add(Arrays.asList("alpha", Float.class, "The alpha value for this image", false, 1F));
            rows.add(Arrays.asList("directions", Direction[].class, "Array of directions it will render in", false, null));
            return rows;
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public JsonElement getExampleJson() {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("texture", "namespace:path/to/texture.png");
            jsonObject.addProperty("alpha", 1F);
            JsonArray jsonArray = new JsonArray();
            jsonArray.add("up");
            jsonArray.add("west");
            jsonObject.add("directions", jsonArray);
            return jsonObject;
        }
    }
}
