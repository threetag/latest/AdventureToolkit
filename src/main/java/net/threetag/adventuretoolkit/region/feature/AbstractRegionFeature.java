package net.threetag.adventuretoolkit.region.feature;

import net.threetag.adventuretoolkit.region.Region;

public abstract class AbstractRegionFeature implements IRegionFeature {

    protected Region region;

    @Override
    public void setRegion(Region region) {
        this.region = region;
    }
}
