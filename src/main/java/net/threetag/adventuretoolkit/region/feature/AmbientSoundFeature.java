package net.threetag.adventuretoolkit.region.feature;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.threetag.adventuretoolkit.client.audio.AmbientRegionSound;
import net.threetag.threecore.util.documentation.IDocumentationSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AmbientSoundFeature extends AbstractRegionFeature {

    private final ResourceLocation sound;
    private final float volume, pitch;
    private final boolean repeat;
    private final int repeatDelay;

    public AmbientSoundFeature(ResourceLocation sound, float volume, float pitch, boolean repeat, int repeatDelay) {
        this.sound = sound;
        this.volume = volume;
        this.pitch = pitch;
        this.repeat = repeat;
        this.repeatDelay = repeatDelay;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void onEnter(PlayerEntity player) {
        Minecraft.getInstance().getSoundManager().play(new AmbientRegionSound(this.region, this.sound, this.volume, this.pitch, this.repeat, this.repeatDelay));
    }

    @Override
    public IRegionFeatureSerializer getSerializer() {
        return RegionFeatures.AMBIENT_SOUND.get();
    }

    public static class Serializer extends ForgeRegistryEntry<IRegionFeatureSerializer> implements IRegionFeatureSerializer, IDocumentationSettings {

        @Override
        public IRegionFeature read(JsonObject json) {
            ResourceLocation sound = new ResourceLocation(JSONUtils.getAsString(json, "sound"));
            float volume = JSONUtils.getAsFloat(json, "volume", 1F);
            float pitch = JSONUtils.getAsFloat(json, "pitch", 1F);
            boolean repeat = JSONUtils.getAsBoolean(json, "repeat", true);
            int repeatDelay = JSONUtils.getAsInt(json, "repeat_delay", 0);
            return new AmbientSoundFeature(sound, volume, pitch, repeat, repeatDelay);
        }

        @Override
        public IRegionFeature read(PacketBuffer buffer) {
            ResourceLocation sound = buffer.readResourceLocation();
            float volume = buffer.readFloat();
            float pitch = buffer.readFloat();
            boolean repeat = buffer.readBoolean();
            int repeatDelay = buffer.readInt();
            return new AmbientSoundFeature(sound, volume, pitch, repeat, repeatDelay);
        }

        @Override
        public void write(PacketBuffer buffer, IRegionFeature feature) {
            AmbientSoundFeature positionedSoundFeature = (AmbientSoundFeature) feature;
            buffer.writeResourceLocation(positionedSoundFeature.sound);
            buffer.writeFloat(positionedSoundFeature.volume);
            buffer.writeFloat(positionedSoundFeature.pitch);
            buffer.writeBoolean(positionedSoundFeature.repeat);
            buffer.writeInt(positionedSoundFeature.repeatDelay);
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public ResourceLocation getId() {
            return this.getRegistryName();
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public List<String> getColumns() {
            return Arrays.asList("Setting", "Type", "Description", "Required", "Fallback Value");
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public List<Iterable<?>> getRows() {
            List<Iterable<?>> rows = new ArrayList<>();
            rows.add(Arrays.asList("sound", ResourceLocation.class, "The ID for the sound", true, null));
            rows.add(Arrays.asList("volume", Float.class, "Volume of the sound", false, 1F));
            rows.add(Arrays.asList("pitch", Float.class, "Pitch of the sound", false, 1F));
            rows.add(Arrays.asList("repeat", Boolean.class, "If true, the sound will play on an infinite loop", false, true));
            rows.add(Arrays.asList("repeat_delay", Integer.class, "If the sound runs on a loop, this determines the amount of ticks for the breaks inbetween the repeats", false, 0));
            return rows;
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public JsonElement getExampleJson() {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("type", this.getId().toString());
            jsonObject.addProperty("sound", "minecraft:ambient.cave");
            jsonObject.addProperty("volume", 1F);
            jsonObject.addProperty("pitch", 1F);
            jsonObject.addProperty("repeat", true);
            jsonObject.addProperty("repeat_delay", 20);
            return jsonObject;
        }
    }

}
