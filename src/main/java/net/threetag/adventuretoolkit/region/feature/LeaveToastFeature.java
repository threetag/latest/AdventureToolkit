package net.threetag.adventuretoolkit.region.feature;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.threetag.adventuretoolkit.client.gui.toasts.RegionToast;
import net.threetag.threecore.util.documentation.IDocumentationSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LeaveToastFeature extends AbstractRegionFeature {

    private final ITextComponent text;
    private final ResourceLocation texturePath;
    private final int u, v;

    public LeaveToastFeature(ITextComponent text, ResourceLocation texturePath, int u, int v) {
        this.text = text;
        this.texturePath = texturePath;
        this.u = u;
        this.v = v;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void onLeave(PlayerEntity player) {
        Minecraft.getInstance().getToasts().addToast(new RegionToast(this.texturePath, this.u, this.v, this.text != null ? this.text : this.region.getName()));
    }

    @Override
    public IRegionFeatureSerializer getSerializer() {
        return RegionFeatures.LEAVE_TOAST.get();
    }

    public static class Serializer extends ForgeRegistryEntry<IRegionFeatureSerializer> implements IRegionFeatureSerializer, IDocumentationSettings {

        @Override
        public IRegionFeature read(JsonObject json) {
            ITextComponent textComponent = JSONUtils.isValidNode(json, "text") ? ITextComponent.Serializer.fromJson(JSONUtils.getAsJsonObject(json, "text").toString()) : null;
            return new LeaveToastFeature(textComponent, new ResourceLocation(JSONUtils.getAsString(json, "texture")), JSONUtils.getAsInt(json, "u", 0), JSONUtils.getAsInt(json, "v", 0));
        }

        @Override
        public IRegionFeature read(PacketBuffer buffer) {
            boolean hasText = buffer.readBoolean();
            ITextComponent text = hasText ? buffer.readComponent() : null;
            return new LeaveToastFeature(text, buffer.readResourceLocation(), buffer.readInt(), buffer.readInt());
        }

        @Override
        public void write(PacketBuffer buffer, IRegionFeature feature) {
            LeaveToastFeature toastFeature = (LeaveToastFeature) feature;
            buffer.writeBoolean(toastFeature.text != null);
            if (toastFeature.text != null) {
                buffer.writeComponent(toastFeature.text);
            }
            buffer.writeResourceLocation(toastFeature.texturePath);
            buffer.writeInt(toastFeature.u);
            buffer.writeInt(toastFeature.v);
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public ResourceLocation getId() {
            return this.getRegistryName();
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public List<String> getColumns() {
            return Arrays.asList("Setting", "Type", "Description", "Required", "Fallback Value");
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public List<Iterable<?>> getRows() {
            List<Iterable<?>> rows = new ArrayList<>();
            rows.add(Arrays.asList("text", ITextComponent.class, "The text which will be displayed on the toast. If none is specified, it will use the region's name instead.", false, null));
            rows.add(Arrays.asList("texture", ResourceLocation.class, "The path to the texture for the toast background", true, null));
            rows.add(Arrays.asList("u", Integer.class, "U location on the texture", false, 0));
            rows.add(Arrays.asList("v", Integer.class, "V location on the texture", false, 0));
            return rows;
        }

        @OnlyIn(Dist.CLIENT)
        @Override
        public JsonElement getExampleJson() {
            JsonObject jsonObject = new JsonObject();

            JsonObject text = new JsonObject();
            text.addProperty("text", "Toast Text");

            jsonObject.addProperty("type", this.getId().toString());
            jsonObject.add("text", text);
            jsonObject.addProperty("texture", "namespace:path/to/texture.png");
            jsonObject.addProperty("u", 0);
            jsonObject.addProperty("v", 32);

            return jsonObject;
        }
    }

}
