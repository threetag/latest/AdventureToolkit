package net.threetag.adventuretoolkit.region.feature;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.threecore.event.GenerateDocumentationFilesEvent;
import net.threetag.threecore.util.documentation.DocumentationBuilder;
import net.threetag.threecore.util.documentation.IDocumentationSettings;

import java.util.function.Supplier;
import java.util.stream.Collectors;

import static net.threetag.threecore.util.documentation.DocumentationBuilder.heading;

public class RegionFeatures {

    public static final DeferredRegister<IRegionFeatureSerializer> REGION_FEATURES = DeferredRegister.create(IRegionFeatureSerializer.class, AdventureToolkit.MODID);
    @SuppressWarnings("unchecked")
    public static final Supplier<IForgeRegistry<IRegionFeatureSerializer>> REGISTRY = REGION_FEATURES.makeRegistry("region_feature_serializers", () -> new RegistryBuilder().setIDRange(0, 512).setType(IRegionFeatureSerializer.class));

    public static final RegistryObject<IRegionFeatureSerializer> ENTER_TOAST = REGION_FEATURES.register("enter_toast", EnterToastFeature.Serializer::new);
    public static final RegistryObject<IRegionFeatureSerializer> LEAVE_TOAST = REGION_FEATURES.register("leave_toast", LeaveToastFeature.Serializer::new);
    public static final RegistryObject<IRegionFeatureSerializer> POSITIONED_SOUND = REGION_FEATURES.register("positioned_sound", PositionedSoundFeature.Serializer::new);
    public static final RegistryObject<IRegionFeatureSerializer> AMBIENT_SOUND = REGION_FEATURES.register("ambient_sound", AmbientSoundFeature.Serializer::new);
    public static final RegistryObject<IRegionFeatureSerializer> SKY_BOX = REGION_FEATURES.register("sky_box", SkyBoxFeature.Serializer::new);
    public static final RegistryObject<IRegionFeatureSerializer> HORIZON_IMAGE = REGION_FEATURES.register("horizon_image", HorizonImageFeature.Serializer::new);

    @OnlyIn(Dist.CLIENT)
    public static void generateDocumentation(GenerateDocumentationFilesEvent e) {
        new DocumentationBuilder(new ResourceLocation(AdventureToolkit.MODID, "regions/features"), "Region Features")
                .add(heading("Region Features")).addDocumentationSettings(REGISTRY.get().getValues().stream().filter(serializer -> serializer instanceof IDocumentationSettings).map(serializer -> {
            return (IDocumentationSettings) serializer;
        }).collect(Collectors.toList())).save();
    }

}
