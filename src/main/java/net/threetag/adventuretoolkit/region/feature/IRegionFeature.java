package net.threetag.adventuretoolkit.region.feature;

import net.minecraft.entity.player.PlayerEntity;
import net.threetag.adventuretoolkit.region.Region;

public interface IRegionFeature {

    void setRegion(Region region);

    default boolean needsSyncing() {
        return true;
    }

    default void onEnter(PlayerEntity player) {

    }

    default void onLeave(PlayerEntity player) {

    }

    IRegionFeatureSerializer getSerializer();

}
