package net.threetag.adventuretoolkit.region.feature;

import com.google.gson.JsonObject;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.registries.IForgeRegistryEntry;

import javax.annotation.Nullable;

public interface IRegionFeatureSerializer extends IForgeRegistryEntry<IRegionFeatureSerializer> {

    IRegionFeature read(JsonObject json);

    @Nullable
    IRegionFeature read(PacketBuffer buffer);

    void write(PacketBuffer buffer, IRegionFeature feature);

}
