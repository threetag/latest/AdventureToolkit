package net.threetag.adventuretoolkit.region.feature;

import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;

import java.util.List;

public interface ISkyRenderFeature {

    ResourceLocation getTexture();

    float getAlpha();

    List<Direction> getDirections();

}
