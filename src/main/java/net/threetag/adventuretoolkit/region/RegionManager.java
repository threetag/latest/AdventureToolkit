package net.threetag.adventuretoolkit.region;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.JsonReloadListener;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.event.RegionChangeEvent;
import net.threetag.adventuretoolkit.network.SyncRegionsMessage;
import net.threetag.adventuretoolkit.region.feature.IRegionFeature;
import net.threetag.adventuretoolkit.region.feature.IRegionFeatureSerializer;
import net.threetag.adventuretoolkit.region.feature.RegionFeatures;

import java.util.*;

public class RegionManager extends JsonReloadListener {

    private static Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
    private static RegionManager INSTANCE;
    private Map<ResourceLocation, Region> regions = Maps.newHashMap();

    public RegionManager() {
        super(GSON, "regions");
        INSTANCE = this;
    }

    public static RegionManager getInstance() {
        return INSTANCE;
    }

    @Override
    protected void apply(Map<ResourceLocation, JsonElement> objectIn, IResourceManager resourceManagerIn, IProfiler profilerIn) {
        this.regions.clear();
        for (Map.Entry<ResourceLocation, JsonElement> entry : objectIn.entrySet()) {
            ResourceLocation resourcelocation = entry.getKey();
            try {
                Region region = parseRegion(entry.getValue().getAsJsonObject());
                this.regions.put(resourcelocation, region);
            } catch (Exception e) {
                AdventureToolkit.LOGGER.error("Parsing error loading region {}", resourcelocation, e);
            }
        }
        generateChildren(this.regions);
        AdventureToolkit.LOGGER.info("Loaded {} regions", this.regions.size());

        if (ServerLifecycleHooks.getCurrentServer() != null) {
            for (ServerPlayerEntity player : ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayers()) {
                AdventureToolkit.NETWORK_CHANNEL.sendTo(new SyncRegionsMessage(getInstance().regions), player.connection.connection, NetworkDirection.PLAY_TO_CLIENT);
            }
        }
    }

    public static void onWorldJoin(EntityJoinWorldEvent e) {
        if (e.getEntity() instanceof ServerPlayerEntity) {
            AdventureToolkit.NETWORK_CHANNEL.sendTo(new SyncRegionsMessage(getInstance().regions), ((ServerPlayerEntity) e.getEntity()).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
        }
    }

    public static Region parseRegion(JsonObject json) {
        ITextComponent name = ITextComponent.Serializer.fromJson(JSONUtils.getAsJsonObject(json, "name").toString());
        RegistryKey<World> dimension = RegistryKey.create(Registry.DIMENSION_REGISTRY, new ResourceLocation(JSONUtils.getAsString(json, "dimension")));
        List<AxisAlignedBB> area = parseRegionArea(json.get("area"));
        ResourceLocation parent = JSONUtils.isValidNode(json, "parent") ? new ResourceLocation(JSONUtils.getAsString(json, "parent")) : null;
        Region region = new Region(name, dimension, area, parent);

        if (JSONUtils.isValidNode(json, "features")) {
            JsonArray jsonElements = JSONUtils.getAsJsonArray(json, "features");

            for (int i = 0; i < jsonElements.size(); i++) {
                JsonObject jsonObject = jsonElements.get(i).getAsJsonObject();
                region.addFeature(parseRegionFeature(jsonObject));
            }
        }

        return region;
    }

    public static List<AxisAlignedBB> parseRegionArea(JsonElement jsonElement) {
        if (jsonElement.isJsonArray()) {
            JsonArray jsonArray = jsonElement.getAsJsonArray();
            List<AxisAlignedBB> list = Lists.newArrayList();
            for (int i = 0; i < jsonArray.size(); i++) {
                list.addAll(parseRegionArea(jsonArray.get(i)));
            }
            return list;
        } else if (jsonElement.isJsonObject()) {
            return Collections.singletonList(new AxisAlignedBB(JSONUtils.getAsFloat(jsonElement.getAsJsonObject(), "min_x"),
                    JSONUtils.getAsFloat(jsonElement.getAsJsonObject(), "min_y"),
                    JSONUtils.getAsFloat(jsonElement.getAsJsonObject(), "min_z"),
                    JSONUtils.getAsFloat(jsonElement.getAsJsonObject(), "max_x"),
                    JSONUtils.getAsFloat(jsonElement.getAsJsonObject(), "max_y"),
                    JSONUtils.getAsFloat(jsonElement.getAsJsonObject(), "max_z")));
        } else {
            throw new JsonParseException("Region area must be either an array or object!");
        }
    }

    public static IRegionFeature parseRegionFeature(JsonObject json) {
        ResourceLocation id = new ResourceLocation(JSONUtils.getAsString(json, "type"));
        IRegionFeatureSerializer serializer = RegionFeatures.REGISTRY.get().getValue(id);

        if (serializer == null) {
            throw new JsonParseException("No such region feature: " + id.toString());
        }

        return serializer.read(json);
    }

    public static void generateChildren(Map<ResourceLocation, Region> regions) {
        for (Region region : regions.values()) {
            if (region.getParent() != null) {
                Region parent = regions.get(region.getParent());
                if (parent != null && parent != region) {
                    parent.addChild(region);
                }
            }
        }
    }

    @OnlyIn(Dist.CLIENT)
    @Mod.EventBusSubscriber(modid = AdventureToolkit.MODID, value = Dist.CLIENT)
    public static class Client {

        public static Map<ResourceLocation, Region> REGIONS = new HashMap<>();
        public static Region CURRENT_REGION = null;

        @SubscribeEvent
        public static void onClientTick(TickEvent.ClientTickEvent e) {
            if (Minecraft.getInstance().player != null && e.phase == TickEvent.Phase.END) {
                Region previous = CURRENT_REGION;
                CURRENT_REGION = null;
                for (Region region : REGIONS.values()) {
                    Region r = region.isInRegion(Minecraft.getInstance().player);
                    if (r != null) {
                        CURRENT_REGION = r;
                        break;
                    }
                }
                if (previous != CURRENT_REGION) {
                    if (previous != null)
                        previous.onLeave(Minecraft.getInstance().player);
                    if (CURRENT_REGION != null)
                        CURRENT_REGION.onEnter(Minecraft.getInstance().player);
                    MinecraftForge.EVENT_BUS.post(new RegionChangeEvent(Minecraft.getInstance().player, previous, CURRENT_REGION));
                }
            }
        }

    }
}
