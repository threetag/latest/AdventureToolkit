package net.threetag.adventuretoolkit.entity;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;

/**
 * Created by Swirtzly
 * on 22/03/2020 @ 15:12
 */
public class NPCEntity extends CreatureEntity {

    private static final DataParameter<String> SKIN_64 = EntityDataManager.defineId(NPCEntity.class, DataSerializers.STRING);
    private static final DataParameter<Boolean> ALEX = EntityDataManager.defineId(NPCEntity.class, DataSerializers.BOOLEAN);


    public NPCEntity(EntityType<? extends NPCEntity> entityType, World world) {
        super(entityType, world);
    }

    public NPCEntity(World world) {
        this(null, world);
//        this(ATEntityTypes.NPC.get(), world);
    }

    @Override
    protected void defineSynchedData() {
        super.defineSynchedData();
        getEntityData().define(SKIN_64, "NULL");
        getEntityData().define(ALEX, true);
    }

    public boolean isAlex() {
        return getEntityData().get(ALEX);
    }

    public void setIsAlex(boolean alex) {
        getEntityData().set(ALEX, alex);
    }

    public String getSkin64() {
        return getEntityData().get(SKIN_64);
    }

    public void setSkin64(String skin64) {
        getEntityData().set(SKIN_64, skin64);
    }
}
