package net.threetag.adventuretoolkit.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.client.renderer.entity.NPCRenderer;

import java.util.function.Supplier;

public class ATEntityTypes {

    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, AdventureToolkit.MODID);

//    public static final RegistryObject<EntityType<NPCEntity>> NPC = register("npc", () -> EntityType.Builder.<NPCEntity>create(NPCEntity::new, EntityClassification.AMBIENT).size(0.6F, 1.8F));

    @OnlyIn(Dist.CLIENT)
    public static void initRenderers() {
//        RenderingRegistry.registerEntityRenderingHandler(NPC.get(), NPCRenderer::new);
    }

    public static <T extends Entity> RegistryObject<EntityType<T>> register(String id, Supplier<EntityType.Builder<T>> builderSupplier) {
        return ENTITIES.register(id, () -> builderSupplier.get().build(AdventureToolkit.MODID + ":" + id));
    }
}
