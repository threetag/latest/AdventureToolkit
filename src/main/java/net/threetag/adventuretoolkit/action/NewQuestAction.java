package net.threetag.adventuretoolkit.action;

import com.google.gson.JsonObject;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.threetag.adventuretoolkit.capability.QuestHolderCapability;
import net.threetag.adventuretoolkit.quest.Quest;
import net.threetag.adventuretoolkit.quest.QuestManager;

public class NewQuestAction implements IAction {

    private final ResourceLocation questId;

    public NewQuestAction(JsonObject json) {
        this.questId = new ResourceLocation(JSONUtils.getAsString(json, "quest"));
    }

    @Override
    public void accept(PlayerEntity player) {
        player.getCapability(QuestHolderCapability.QUEST_HOLDER).ifPresent(questHolder -> {
            Quest newQuest = QuestManager.getInstance().getQuestById(this.questId);

            if (newQuest != null) {
                questHolder.startQuest(newQuest, player);
            }
        });
    }
}
