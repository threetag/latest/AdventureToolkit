package net.threetag.adventuretoolkit.action;

import net.minecraft.entity.player.PlayerEntity;

import java.util.function.Consumer;

public interface IAction extends Consumer<PlayerEntity> {

}
