package net.threetag.adventuretoolkit.action;

import com.google.gson.JsonObject;
import net.minecraft.command.CommandSource;
import net.minecraft.command.ICommandSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.server.ServerWorld;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.threecore.util.threedata.CommandListThreeData;

import java.util.UUID;

public class CommandAction implements IAction, ICommandSource {

    private final CommandListThreeData.CommandList commandList;

    public CommandAction(JsonObject json) {
        this.commandList = new CommandListThreeData.CommandList(json.get("command"));
    }

    @Override
    public void accept(PlayerEntity player) {
        for (String command : this.commandList.getCommands()) {
            player.level.getServer().getCommands().performCommand(new CommandSource(this, player.position(), player.getRotationVector(), player.level instanceof ServerWorld ? (ServerWorld) player.level : null, 4, player.getName().getString(), player.getDisplayName(), player.level.getServer(), player), command);
        }
    }

    @Override
    public void sendMessage(ITextComponent component, UUID senderUUID) {
        AdventureToolkit.LOGGER.error("Action command error: " + component.getString());
    }

    @Override
    public boolean acceptsSuccess() {
        return false;
    }

    @Override
    public boolean acceptsFailure() {
        return true;
    }

    @Override
    public boolean shouldInformAdmins() {
        return false;
    }
}
