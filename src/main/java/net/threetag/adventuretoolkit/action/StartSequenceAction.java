package net.threetag.adventuretoolkit.action;

import com.google.gson.JsonObject;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.threetag.adventuretoolkit.capability.SequenceHolderCapability;
import net.threetag.adventuretoolkit.sequence.AbstractSequence;
import net.threetag.adventuretoolkit.sequence.SequenceManager;

public class StartSequenceAction implements IAction {

    private final ResourceLocation sequenceId;

    public StartSequenceAction(JsonObject json) {
        this.sequenceId = new ResourceLocation(JSONUtils.getAsString(json, "sequence"));
    }

    @Override
    public void accept(PlayerEntity player) {
        player.getCapability(SequenceHolderCapability.SEQUENCE_HOLDER).ifPresent(sequenceHolder -> {
            AbstractSequence sequence = SequenceManager.INSTANCE.getSequence(this.sequenceId);

            if (sequence != null && player instanceof ServerPlayerEntity) {
                sequenceHolder.startSequence(sequence, (ServerPlayerEntity) player);
            }
        });
    }
}
