package net.threetag.adventuretoolkit.action;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.threetag.adventuretoolkit.AdventureToolkit;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Actions {

    private static final Map<ResourceLocation, Function<JsonObject, IAction>> ACTIONS = new HashMap<>();

    static {
        register(new ResourceLocation(AdventureToolkit.MODID, "item"), ItemAction::new);
        register(new ResourceLocation(AdventureToolkit.MODID, "command"), CommandAction::new);
        register(new ResourceLocation(AdventureToolkit.MODID, "new_quest"), NewQuestAction::new);
        register(new ResourceLocation(AdventureToolkit.MODID, "start_sequence"), StartSequenceAction::new);
        register(new ResourceLocation(AdventureToolkit.MODID, "experience"), ExperienceAction::new);
    }

    public static void register(ResourceLocation id, Function<JsonObject, IAction> function) {
        ACTIONS.put(id, function);
    }

    public static IAction parse(JsonObject json) {
        ResourceLocation type = new ResourceLocation(JSONUtils.getAsString(json, "type"));

        if (!ACTIONS.containsKey(type)) {
            throw new JsonParseException("No such action type: " + type.toString());
        }

        return ACTIONS.get(type).apply(json);
    }

}
