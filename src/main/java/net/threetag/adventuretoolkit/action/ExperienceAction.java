package net.threetag.adventuretoolkit.action;

import com.google.gson.JsonObject;
import net.minecraft.entity.player.PlayerEntity;
import net.threetag.threecore.util.threedata.ExperienceThreeData;

public class ExperienceAction implements IAction {

    private final ExperienceThreeData.Experience experience;

    public ExperienceAction(JsonObject json) {
        this.experience = new ExperienceThreeData.Experience(json.get("xp").getAsJsonPrimitive());
    }

    @Override
    public void accept(PlayerEntity player) {
        this.experience.give(player);
    }
}
