package net.threetag.adventuretoolkit.action;

import com.google.gson.JsonObject;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.crafting.CraftingHelper;

public class ItemAction implements IAction {

    private final ItemStack stack;

    public ItemAction(ItemStack stack) {
        this.stack = stack;
    }

    public ItemAction(JsonObject json) {
        this.stack = CraftingHelper.getItemStack(json, true);
    }

    @Override
    public void accept(PlayerEntity player) {
        if (!player.inventory.add(stack.copy())) {
            player.spawnAtLocation(this.stack.copy());
        }
    }

}
