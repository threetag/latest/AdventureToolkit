package net.threetag.adventuretoolkit.command;

import com.google.common.collect.Lists;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.command.arguments.ResourceLocationArgument;
import net.minecraft.command.arguments.SuggestionProviders;
import net.minecraft.command.arguments.Vec3Argument;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.network.StartTeleportTransitionMessage;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Mod.EventBusSubscriber(modid = AdventureToolkit.MODID)
public class TransitionalTeleportCommand {

    public static List<Teleport> TELEPORTS = Lists.newArrayList();

    public static void register(CommandDispatcher<CommandSource> dispatcher) {
        TELEPORTS.clear();
        dispatcher.register(Commands.literal("transitionalteleport").requires((commandSource) -> {
            return commandSource.hasPermission(2);
        }).then(Commands.argument("entities", EntityArgument.entities()).then(Commands.argument("location", Vec3Argument.vec3()).then(Commands.argument("sound", ResourceLocationArgument.id()).suggests(SuggestionProviders.AVAILABLE_SOUNDS).then(Commands.argument("duration", IntegerArgumentType.integer(0, 20 * 60)).executes(context -> {
            return startTeleport(context.getSource(), EntityArgument.getEntities(context, "entities"), Vec3Argument.getVec3(context, "location"), ResourceLocationArgument.getId(context, "sound"), IntegerArgumentType.getInteger(context, "duration"));
        }))))));
    }

    @SubscribeEvent
    public static void onTick(TickEvent.ServerTickEvent e) {
        TELEPORTS = TELEPORTS.stream().filter(Teleport::tick).collect(Collectors.toList());
    }

    public static int startTeleport(CommandSource commandSource, Collection<? extends Entity> entities, Vector3d pos, ResourceLocation soundEvent, int duration) {
        try {
            for (Entity entity : entities) {
                TELEPORTS.add(new Teleport(entity, pos, ForgeRegistries.SOUND_EVENTS.getValue(soundEvent), duration));
                if (entity instanceof ServerPlayerEntity) {
                    AdventureToolkit.NETWORK_CHANNEL.sendTo(new StartTeleportTransitionMessage(duration, ForgeRegistries.SOUND_EVENTS.getValue(soundEvent)), ((ServerPlayerEntity) entity).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
                }
            }

            if (entities.size() == 1) {
                commandSource.sendSuccess(new TranslationTextComponent("commands.teleport.success.location.single", entities.iterator().next().getDisplayName(), pos.x, pos.y, pos.z), true);
            } else {
                commandSource.sendSuccess(new TranslationTextComponent("commands.teleport.success.location.multiple", entities.size(), pos.x, pos.y, pos.z), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return entities.size();
    }

    static class Teleport {

        private final Entity entity;
        private final Vector3d pos;
        private final SoundEvent soundEvent;
        private final int duration;
        private int progress = 0;

        public Teleport(Entity entity, Vector3d pos, SoundEvent soundEvent, int duration) {
            this.entity = entity;
            this.pos = pos;
            this.soundEvent = soundEvent;
            this.duration = duration;
        }

        public boolean tick() {
            this.progress++;

            if (this.progress >= this.duration) {
                return false;
            }

            if (this.progress == this.duration / 2 + 1) {
                this.entity.teleportTo(this.pos.x(), this.pos.y(), this.pos.z());
            }

            return true;
        }
    }

}
