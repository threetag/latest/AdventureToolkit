package net.threetag.adventuretoolkit.command;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.threetag.adventuretoolkit.money.MoneyAPI;

public class MoneyCommand {

    public static void register(CommandDispatcher<CommandSource> dispatcher) {
        dispatcher.register(Commands.literal("money").executes(context -> getMoney(context.getSource(), context.getSource().getPlayerOrException()))
                .then(Commands.argument("player", EntityArgument.player()).requires((source) -> {
                    return source.hasPermission(2);
                }).executes(context -> {
                    return getMoney(context.getSource(), EntityArgument.getPlayer(context, "player"));
                })).then(Commands.literal("set").requires((source) -> {
                    return source.hasPermission(2);
                }).then(Commands.argument("player", EntityArgument.player()).then(Commands.argument("amount", IntegerArgumentType.integer(0)).executes(context -> {
                    return setMoney(context.getSource(), EntityArgument.getPlayer(context, "player"), IntegerArgumentType.getInteger(context, "amount"));
                })))));
    }

    private static int getMoney(CommandSource source, ServerPlayerEntity player) {
        int money = MoneyAPI.getMoney(player.getGameProfile());
        if (source.getEntity() == player) {
            source.sendSuccess(new TranslationTextComponent("commands.money.balance.self", money), false);
        } else {
            source.sendSuccess(new TranslationTextComponent("commands.money.balance.other", player.getDisplayName(), money), false);
        }
        return money;
    }

    public static int setMoney(CommandSource source, ServerPlayerEntity player, int money) {
        if (MoneyAPI.setMoney(player.getGameProfile(), money, MoneyAPI.TYPE_CHANGED_BY_STAFF, "Changed by Player " + source.getDisplayName().getString())) {
            source.sendSuccess(new TranslationTextComponent("commands.money.set", player.getDisplayName(), money), true);
        } else {
            source.sendFailure(new TranslationTextComponent("commands.money.set.failure", player.getDisplayName(), money));
        }

        return 1;
    }

}
