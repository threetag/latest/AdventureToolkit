package net.threetag.adventuretoolkit.mixin;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.WorldRenderer;
import net.threetag.adventuretoolkit.client.renderer.SkyBoxRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(WorldRenderer.class)
public class WorldRendererMixin {

    @Inject(at = @At("TAIL"), method = "renderSky(Lcom/mojang/blaze3d/matrix/MatrixStack;F)V")
    public void renderSkyTail(MatrixStack matrixStackIn, float partialTicks, CallbackInfo callbackInfo) {
        SkyBoxRenderer.postRender(matrixStackIn, partialTicks);
    }

    @Inject(at = @At("HEAD"), method = "renderSky(Lcom/mojang/blaze3d/matrix/MatrixStack;F)V", cancellable = true)
    public void renderSkyHead(MatrixStack matrixStackIn, float partialTicks, CallbackInfo callbackInfo) {
        SkyBoxRenderer.preRender(matrixStackIn, partialTicks, callbackInfo);
    }

}
