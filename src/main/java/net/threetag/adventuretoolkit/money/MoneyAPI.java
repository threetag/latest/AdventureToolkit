package net.threetag.adventuretoolkit.money;

import com.google.common.base.Preconditions;
import com.mojang.authlib.GameProfile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nullable;

public class MoneyAPI {

    private static final Logger LOGGER = LogManager.getLogger();

    private static IMoneyHandler moneyHandler = DefaultMoneyHandler.INSTANCE;

    public static final int TYPE_UNKNOWN = 0;
    public static final int TYPE_SENT_TO = 1;
    public static final int TYPE_RECEIVED_FROM = 2;
    public static final int TYPE_CHANGED_BY_STAFF = 3;
    public static final int TYPE_QUEST_REWARD = 4;
    public static final int TYPE_SOLD_ITEMS = 5;
    public static final int TYPE_TELEPORT_COST = 6;
    public static final int TYPE_CLAIM_COST = 7;

    public static void setMoneyHandler(IMoneyHandler handler) {
        Preconditions.checkNotNull(handler, "Money handler can't be null!");
        LOGGER.warn("Replacing {} with {}", moneyHandler.getClass().getName(), handler.getClass().getName());
        moneyHandler = handler;
    }

    public static IMoneyHandler getMoneyHandler() {
        return moneyHandler;
    }

    public static boolean setMoney(GameProfile profile, int amount, int type, @Nullable String description) {
        Preconditions.checkNotNull(profile, "GameProfile can't be null!");
        return moneyHandler.setMoney(profile, amount, type, description);
    }

    public static boolean setMoney(GameProfile profile, int amount) {
        return setMoney(profile, amount, TYPE_UNKNOWN, null);
    }

    public static int getMoney(GameProfile profile) {
        return moneyHandler.getMoney(profile);
    }

}
