package net.threetag.adventuretoolkit.money;

import com.mojang.authlib.GameProfile;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.threetag.threecore.capability.CapabilityThreeData;
import net.threetag.threecore.event.RegisterThreeDataEvent;
import net.threetag.threecore.util.threedata.EnumSync;
import net.threetag.threecore.util.threedata.IntegerThreeData;
import net.threetag.threecore.util.threedata.ThreeData;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class DefaultMoneyHandler implements IMoneyHandler {

    public static final DefaultMoneyHandler INSTANCE = new DefaultMoneyHandler();
    public static final ThreeData<Integer> MONEY_THREE_DATA = new IntegerThreeData("money").setSyncType(EnumSync.SELF);

    @Override
    public boolean setMoney(GameProfile profile, int amount, int type, String description) {
        ServerPlayerEntity player = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayer(profile.getId());

        if (player != null) {
            AtomicBoolean result = new AtomicBoolean(false);
            player.getCapability(CapabilityThreeData.THREE_DATA).ifPresent(holder -> {
                if (holder.has(MONEY_THREE_DATA)) {
                    holder.set(MONEY_THREE_DATA, amount);
                    result.set(true);
                }
            });
            return result.get();
        } else {
            return false;
        }
    }

    @Override
    public int getMoney(GameProfile profile) {
        ServerPlayerEntity player = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayer(profile.getId());

        if (player != null) {
            AtomicInteger result = new AtomicInteger(0);
            player.getCapability(CapabilityThreeData.THREE_DATA).ifPresent(holder -> {
                if (holder.has(MONEY_THREE_DATA)) {
                    result.set(holder.get(MONEY_THREE_DATA));
                }
            });
            return result.get();
        } else {
            return 0;
        }
    }

    public static void onRegisterThreeData(RegisterThreeDataEvent e) {
        if (e.getEntity() instanceof PlayerEntity) {
            e.register(MONEY_THREE_DATA, 0);
        }
    }
}
