package net.threetag.adventuretoolkit.money;

import com.mojang.authlib.GameProfile;

import javax.annotation.Nullable;

public interface IMoneyHandler {

    boolean setMoney(GameProfile profile, int amount, int type, @Nullable String description);

    int getMoney(GameProfile profile);

}
