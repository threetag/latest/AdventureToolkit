package net.threetag.adventuretoolkit.script.accessor;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.threetag.adventuretoolkit.quest.IQuestHolder;
import net.threetag.adventuretoolkit.quest.Quest;
import net.threetag.adventuretoolkit.quest.QuestManager;
import net.threetag.threecore.scripts.ScriptParameterName;
import net.threetag.threecore.scripts.accessors.ScriptAccessor;

public class QuestHandlerAccessor extends ScriptAccessor<IQuestHolder> {

    private final PlayerEntity playerEntity;

    public QuestHandlerAccessor(IQuestHolder value, PlayerEntity playerEntity) {
        super(value);
        this.playerEntity = playerEntity;
    }

    public boolean startQuest(@ScriptParameterName("questId") String questId) {
        ResourceLocation id = new ResourceLocation(questId);
        Quest quest = QuestManager.getInstance().getQuestById(id);
        if (quest == null) {
            return false;
        } else {
            return this.value.startQuest(quest, this.playerEntity);
        }
    }

    public String[] getActiveQuests() {
        String[] quests = new String[this.value.getActiveQuests().size()];
        for (int i = 0; i < quests.length; i++) {
            quests[i] = this.value.getActiveQuests().get(i).getQuest().getId().toString();
        }
        return quests;
    }

    public String[] getCompletedQuests() {
        String[] quests = new String[this.value.getCompletedQuests().size()];
        for (int i = 0; i < quests.length; i++) {
            quests[i] = this.value.getCompletedQuests().get(i).getId().toString();
        }
        return quests;
    }

    public void clearQuests() {
        this.value.clearQuests();
    }
}
