package net.threetag.adventuretoolkit.script.binding;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.threetag.adventuretoolkit.capability.QuestHolderCapability;
import net.threetag.adventuretoolkit.capability.SequenceHolderCapability;
import net.threetag.adventuretoolkit.script.accessor.QuestHandlerAccessor;
import net.threetag.adventuretoolkit.sequence.AbstractSequence;
import net.threetag.adventuretoolkit.sequence.SequenceManager;
import net.threetag.threecore.scripts.ScriptParameterName;
import net.threetag.threecore.scripts.accessors.LivingEntityAccessor;

import java.util.concurrent.atomic.AtomicReference;

public class ATScriptBinding {

    public QuestHandlerAccessor getQuestHandler(@ScriptParameterName("player") LivingEntityAccessor player) {
        AtomicReference<QuestHandlerAccessor> accessor = new AtomicReference<>();
        player.value.getCapability(QuestHolderCapability.QUEST_HOLDER).ifPresent(holder -> accessor.set(new QuestHandlerAccessor(holder, (PlayerEntity) player.value)));
        return accessor.get();
    }

    public String getCurrentSequence(@ScriptParameterName("player") LivingEntityAccessor player) {
        AtomicReference<String> id = new AtomicReference<>();
        player.value.getCapability(SequenceHolderCapability.SEQUENCE_HOLDER).ifPresent(sequenceHolder -> {
            id.set(sequenceHolder.getCurrentSequence() != null ? sequenceHolder.getCurrentSequence().getId().toString() : null);
        });
        return id.get();
    }

    public boolean startSequence(@ScriptParameterName("player") LivingEntityAccessor player, @ScriptParameterName("sequenceId") String sequenceId) {
        ResourceLocation id = new ResourceLocation(sequenceId);
        AbstractSequence sequence = SequenceManager.getInstance().getSequence(id);
        if (sequence == null || !(player.value instanceof ServerPlayerEntity) || !player.value.getCapability(SequenceHolderCapability.SEQUENCE_HOLDER).isPresent()) {
            return false;
        } else {
            player.value.getCapability(SequenceHolderCapability.SEQUENCE_HOLDER).ifPresent(sequenceHolder -> {
                sequenceHolder.startSequence(sequence, (ServerPlayerEntity) player.value);
            });
            return true;
        }
    }

    public void stopCurrentSequence(@ScriptParameterName("player") LivingEntityAccessor player) {
        if (player.value instanceof ServerPlayerEntity) {
            player.value.getCapability(SequenceHolderCapability.SEQUENCE_HOLDER).ifPresent(sequenceHolder -> {
                sequenceHolder.stopCurrentSequence((ServerPlayerEntity) player.value);
            });
        }
    }

}
