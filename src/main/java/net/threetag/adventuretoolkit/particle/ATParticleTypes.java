package net.threetag.adventuretoolkit.particle;

import net.minecraft.client.Minecraft;
import net.minecraft.particles.BasicParticleType;
import net.minecraft.particles.ParticleType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.adventuretoolkit.AdventureToolkit;

public class ATParticleTypes {

    public static final DeferredRegister<ParticleType<?>> PARTICLE_TYPES = DeferredRegister.create(ForgeRegistries.PARTICLE_TYPES, AdventureToolkit.MODID);

    public static final RegistryObject<BasicParticleType> LIGHT_SOURCE = PARTICLE_TYPES.register("light_source", () -> new BasicParticleType(false));

    @OnlyIn(Dist.CLIENT)
    public static void registerFactories() {
        Minecraft.getInstance().particleEngine.register(LIGHT_SOURCE.get(), new LightSourceParticle.Factory());
    }

}
