package net.threetag.adventuretoolkit.particle;


import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.IParticleFactory;
import net.minecraft.client.particle.IParticleRenderType;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.SpriteTexturedParticle;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;
import net.minecraft.util.IItemProvider;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.threetag.adventuretoolkit.block.ATBlocks;

@OnlyIn(Dist.CLIENT)
public class LightSourceParticle extends SpriteTexturedParticle {

    private LightSourceParticle(ClientWorld world, double x, double y, double z, IItemProvider itemProvider) {
        super(world, x, y, z);
        this.setSprite(Minecraft.getInstance().getItemRenderer().getItemModelShaper().getParticleIcon(itemProvider));
        this.gravity = 0.0F;
        this.lifetime = 80;
        this.hasPhysics = false;
    }

    public IParticleRenderType getRenderType() {
        return IParticleRenderType.TERRAIN_SHEET;
    }

    public float getQuadSize(float scaleFactor) {
        return 0.5F;
    }

    @OnlyIn(Dist.CLIENT)
    public static class Factory implements IParticleFactory<BasicParticleType> {

        public Particle createParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed) {
            return new LightSourceParticle(worldIn, x, y, z, ATBlocks.LIGHT_SOURCE_BLOCK.get().asItem());
        }

    }
}
