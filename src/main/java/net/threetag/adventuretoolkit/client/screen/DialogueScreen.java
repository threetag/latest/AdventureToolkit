package net.threetag.adventuretoolkit.client.screen;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.ITextProperties;
import net.minecraft.util.text.Style;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.network.StopSequenceMessage;
import net.threetag.threecore.util.icon.IIcon;

import java.util.List;

public class DialogueScreen extends Screen {

    public static final ResourceLocation TEXTURE = new ResourceLocation(AdventureToolkit.MODID, "textures/gui/dialogue.png");

    private final ITextComponent text;
    private final IIcon background;
    private final IIcon foreground;
    private List<List<ITextProperties>> pages;
    private int page = 0;
    private int textProgress = 0;
    private int time = 0;

    private final int xSize = 512;
    private final int ySize = 132;

    public DialogueScreen(ITextComponent name, ITextComponent text, IIcon background, IIcon foreground) {
        super(name);
        this.text = text;
        this.background = background;
        this.foreground = foreground;
    }

    @Override
    protected void init() {
        super.init();
        this.pages = Lists.partition(this.font.getSplitter().splitLines(this.text, 350, Style.EMPTY), 6);
        this.page = 0;
        this.textProgress = 0;
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
        if (keyCode == 257) {
            if (this.textProgress < 0) {
                if (this.page < this.pages.size() - 1) {
                    this.page++;
                    this.textProgress = 0;
                } else {
                    this.onClose();
                }
            } else {
                this.textProgress = Integer.MAX_VALUE;
            }
            return true;
        } else {
            return super.keyPressed(keyCode, scanCode, modifiers);
        }
    }

    @Override
    public void tick() {
        if (this.textProgress >= 0) {
            this.textProgress++;
        }
        this.time++;
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.getMinecraft().getTextureManager().bind(TEXTURE);
        int left = (this.width - this.xSize) / 2;
        int top = (this.height - this.ySize) - 30;
        blit(matrixStack, left, top, 0, 0, this.xSize, this.ySize, 512, 256);
        if(this.textProgress < 0) {
            blit(matrixStack, left + 494, top + 112 - (this.time / 15 % 2 == 0 ? 1 : 0), 0, 132, 6, 4, 512, 256);
        }

        if (this.background != null) {
            RenderSystem.pushMatrix();
            RenderSystem.translatef(left + 18, top + 18, 0);
            RenderSystem.scalef(2F, 2F, 1F);
            this.foreground.draw(this.minecraft, matrixStack);
            RenderSystem.popMatrix();
        } else {
            AbstractGui.fill(matrixStack, left + 18, top + 18, left + 114, top + 114, 0xff009aff);
        }

        if (this.foreground != null) {
            RenderSystem.pushMatrix();
            RenderSystem.translatef(left + 18, top + 18, 0);
            RenderSystem.scalef(2F, 2F, 1F);
            this.foreground.draw(this.minecraft, matrixStack);
            RenderSystem.popMatrix();
        }

        int length = this.font.width(this.title);
        this.font.draw(matrixStack, this.title, left + 66 - length / 2F, top + 5, 4210752);

        List<ITextProperties> lines = this.pages.get(this.page);
        int j = this.textProgress;
        int k = 0;
        for (int i = 0; i < lines.size(); i++) {
            ITextProperties p = lines.get(i);
            String line = p.getString();
            k += line.length();
            if(textProgress < 0) {
                this.font.draw(matrixStack, line, left + 142, top + 69 - (lines.size() * 15 / 2F) + i * 15, 0xfefefe);
            } else if (j > 0) {
                line = line.substring(0, MathHelper.clamp(j, 0, line.length()));
                this.font.draw(matrixStack, line, left + 142, top + 69 - (lines.size() * 15 / 2F) + i * 15, 0xfefefe);
            }
            j -= line.length();
        }
        if(this.textProgress >= k) {
            this.textProgress = -1;
        }

        super.render(matrixStack, mouseX, mouseY, partialTicks);
    }

    @Override
    public void removed() {
        AdventureToolkit.NETWORK_CHANNEL.sendToServer(new StopSequenceMessage());
    }
}
