package net.threetag.adventuretoolkit.client.screen;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.list.ExtendedList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.capability.QuestHolderCapability;
import net.threetag.adventuretoolkit.quest.QuestInstance;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ActiveQuestsScreen extends Screen {

    public static final ResourceLocation TEXTURE = new ResourceLocation(AdventureToolkit.MODID, "textures/gui/active_quests.png");
    public static final ResourceLocation TEXTURE_WIDGETS = new ResourceLocation(AdventureToolkit.MODID, "textures/gui/quests_widgets.png");

    public QuestList questList;
    private final int xSize = 213;
    private final int ySize = 244;

    public ActiveQuestsScreen() {
        super(new TranslationTextComponent("gui.quests.active"));
    }

    @Override
    protected void init() {
        super.init();
        int left = (this.width - this.xSize) / 2;
        int top = (this.height - this.ySize) / 2;
        this.questList = new QuestList(this.minecraft, 171 + left + 19, 176, top + 36, top + 208, 31);
        this.questList.setLeftPos(left + 19);
        this.questList.setRenderBackground(false);
        this.questList.setRenderTopAndBottom(false);
        this.children.add(questList);
    }

    @Override
    public void render(MatrixStack stack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(stack);

        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);

        if (this.questList != null)
            this.questList.render(stack, mouseX, mouseY, partialTicks);

        RenderSystem.pushMatrix();
        RenderSystem.translatef(0, 0, 100);
        this.getMinecraft().getTextureManager().bind(TEXTURE);
        int left = (this.width - this.xSize) / 2;
        int top = (this.height - this.ySize) / 2;
        this.blit(stack, left, top, 0, 0, this.xSize, this.ySize);
        RenderSystem.popMatrix();

        super.render(stack, mouseX, mouseY, partialTicks);
    }

    public class QuestList extends ExtendedList<QuestListEntry> {

        public QuestList(Minecraft mcIn, int widthIn, int heightIn, int topIn, int bottomIn, int slotHeightIn) {
            super(mcIn, widthIn, heightIn, topIn, bottomIn, slotHeightIn);
            this.centerListVertically = true;
            List<QuestListEntry> entries = new ArrayList<>();
            mcIn.player.getCapability(QuestHolderCapability.QUEST_HOLDER).ifPresent(questHolder -> {
                questHolder.getActiveQuests().forEach(questInstance -> entries.add(new QuestListEntry(questInstance)));
            });
            entries.sort(Comparator.comparingInt(e -> e.quest.getQuest().getType().ordinal()));
            this.replaceEntries(entries);
        }

        @Override
        protected void renderBackground(MatrixStack matrixStack) {
            AbstractGui.fill(matrixStack, this.x0, this.y0, this.width + 5, this.y1, 0xff868686);
        }

        @Override
        public int getRowWidth() {
            return this.width;
        }

        @Override
        protected int getScrollbarPosition() {
            return this.width;
        }
    }

    public static class QuestListEntry extends ExtendedList.AbstractListEntry<QuestListEntry> {

        public final QuestInstance quest;

        public QuestListEntry(QuestInstance quest) {
            this.quest = quest;
        }

        @Override
        public void render(MatrixStack matrixStack, int entryIdx, int top, int left, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean isMouseOver, float partialTicks) {
            Minecraft.getInstance().getTextureManager().bind(TEXTURE_WIDGETS);
            AbstractGui.blit(matrixStack, left + 2, top + 2, 0, isMouseOver ? 81 : quest.getQuest().getType().getButtonTextureV(), 167, 27, 256, 256);
            AbstractGui.blit(matrixStack, left + 5, top + 2 + 19, 0, 108, 161, 5, 256, 256);
            AbstractGui.blit(matrixStack, left + 5, top + 2 + 19, 0, 113, (int) (161F * this.quest.getTotalProgress()), 5, 256, 256);
            FontRenderer fontRenderer = Minecraft.getInstance().font;
            fontRenderer.drawShadow(matrixStack, fontRenderer.split(this.quest.getQuest().getTitle(), entryWidth - 10).get(0), left + 6, top + 7, isMouseOver ? 16777120 : 0xfefefe);
        }

        @Override
        public boolean mouseClicked(double mouseX, double mouseY, int type) {
//            Minecraft.getInstance().getSoundHandler().play(SimpleSound.master(SoundEvents.UI_BUTTON_CLICK, 1.0F));
            Minecraft.getInstance().setScreen(new QuestScreen(this.quest));
            return true;
        }
    }
}
