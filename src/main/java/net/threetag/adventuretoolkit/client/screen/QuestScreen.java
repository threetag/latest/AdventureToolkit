package net.threetag.adventuretoolkit.client.screen;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IReorderingProcessor;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.gui.ScrollPanel;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.quest.QuestInstance;
import net.threetag.adventuretoolkit.quest.requirement.progress.AbstractQuestRequirementProgress;

import java.util.List;
import java.util.Map;

public class QuestScreen extends Screen {

    public static final ResourceLocation TEXTURE = new ResourceLocation(AdventureToolkit.MODID, "textures/gui/quest_information.png");

    private Panel panel;
    private final QuestInstance questInstance;
    private final int xSize = 512;
    private final int ySize = 256;
    private int guiLeft;
    private int guiTop;

    public QuestScreen(QuestInstance questInstance) {
        super(questInstance.getQuest().getTitle());
        this.questInstance = questInstance;
    }

    @Override
    protected void init() {
        super.init();
        this.guiLeft = (this.width - xSize) / 2;
        this.guiTop = (this.height - ySize) / 2;
        this.panel = new Panel(this.minecraft, 476, 220, guiTop + 18, guiLeft + 18, this.questInstance);
        this.children.add(this.panel);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        AbstractGui.fill(matrixStack, guiLeft + 5, guiTop + 5, guiLeft + xSize - 10, guiTop + ySize - 10, 0xff868686);
        this.getMinecraft().getTextureManager().bind(TEXTURE);
        blit(matrixStack, guiLeft, guiTop, 0, 0, xSize, ySize, xSize, ySize);
        if (this.panel != null)
            this.panel.render(matrixStack, mouseX, mouseY, partialTicks);

        int length = this.font.width(this.questInstance.getQuest().getTitle().getString());
        int startX = (int) (this.guiLeft + this.xSize / 2F - length / 2F - 5) - 5;
        int endX = (int) (this.guiLeft + this.xSize / 2F + length / 2F - 10) + 5;
        this.getMinecraft().getTextureManager().bind(ActiveQuestsScreen.TEXTURE_WIDGETS);
        this.blit(matrixStack, startX, guiTop + 15, 167, 0, 15, 10);
        this.blit(matrixStack, endX, guiTop + 15, 189, 0, 15, 10);
        for (int i = 0; i < Math.ceil((endX - (startX + 15)) / 7F); i++) {
            this.blit(matrixStack, startX + 15 + i * 7, guiTop + 15, 182, 0, 7, 10);
        }
        font.draw(matrixStack, this.questInstance.getQuest().getTitle(), this.guiLeft + this.xSize / 2F - length / 2F, this.guiTop + 10, 4210752);

        super.render(matrixStack, mouseX, mouseY, partialTicks);
    }

    class Panel extends ScrollPanel {

        private final QuestInstance questInstance;
        private final List<IReorderingProcessor> lines;

        public Panel(Minecraft client, int width, int height, int top, int left, QuestInstance questInstance) {
            super(client, width, height, top, left);
            this.questInstance = questInstance;
            this.lines = font.split(this.questInstance.getQuest().getDescription(), this.width - 25);
        }

        @Override
        protected int getContentHeight() {
            return 15 + this.lines.size() * 15 + 15 + 1 + 15 + this.questInstance.getProgresses().size() * 20;
        }

        @Override
        protected void drawPanel(MatrixStack matrixStack, int entryRight, int relativeY, Tessellator tess, int mouseX, int mouseY) {
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            RenderSystem.enableBlend();
            relativeY += 15;
            for (IReorderingProcessor processor : font.split(this.questInstance.getQuest().getDescription(), this.width - 25)) {
                font.draw(matrixStack, processor, left + 10, relativeY, 0xfefefe);
                relativeY += 15;
            }
            RenderSystem.disableAlphaTest();
            RenderSystem.disableBlend();

            AbstractGui.fill(matrixStack, left + 10, relativeY, left + width - 10, ++relativeY, 0xfffefefe);
            relativeY += 15;

            for (Map.Entry<String, AbstractQuestRequirementProgress> entry : this.questInstance.getProgresses().entrySet()) {
                Minecraft.getInstance().getTextureManager().bind(ActiveQuestsScreen.TEXTURE_WIDGETS);
                this.blit(matrixStack, left + width - 35 - 161, relativeY + 1, 0, 108, 161, 5);
                float progress = (float) entry.getValue().getProgress().getFirst() / (float) entry.getValue().getProgress().getSecond();
                this.blit(matrixStack, left + width - 35 - 161, relativeY + 1, 0, 113, (int) (161 * progress), 5);
                IFormattableTextComponent title = this.questInstance.getQuest().getRequirement(entry.getKey()).getTitle().copy();
                if (progress >= 1F) {
                    title.withStyle(TextFormatting.STRIKETHROUGH, TextFormatting.GRAY);
                }
                font.draw(matrixStack, title, left + 10, relativeY, 0xfefefe);
                relativeY += 20;
            }
        }
    }
}
