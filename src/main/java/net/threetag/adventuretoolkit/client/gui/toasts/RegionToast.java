package net.threetag.adventuretoolkit.client.gui.toasts;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.toasts.IToast;
import net.minecraft.client.gui.toasts.ToastGui;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import net.minecraft.client.gui.toasts.IToast.Visibility;

@OnlyIn(Dist.CLIENT)
public class RegionToast implements IToast {

    private final ResourceLocation texture;
    private final int u, v;
    private final ITextComponent name;

    public RegionToast(ResourceLocation texture, int u, int v, ITextComponent name) {
        this.texture = texture;
        this.u = u;
        this.v = v;
        this.name = name;
    }

    @Override
    public Visibility render(MatrixStack matrixStack, ToastGui toastGui, long time) {
        toastGui.getMinecraft().getTextureManager().bind(this.texture);
        RenderSystem.color3f(1.0F, 1.0F, 1.0F);
        toastGui.blit(matrixStack, 0, 0, this.u, this.v, this.width(), this.height());
        toastGui.getMinecraft().font.draw(matrixStack, this.name, this.width() - toastGui.getMinecraft().font.width(this.name.getString()) - 8, 12.0F, -11534256);
        return time >= 5000L ? Visibility.HIDE : Visibility.SHOW;
    }
}
