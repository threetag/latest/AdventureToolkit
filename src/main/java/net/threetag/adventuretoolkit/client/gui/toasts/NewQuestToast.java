package net.threetag.adventuretoolkit.client.gui.toasts;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.toasts.IToast;
import net.minecraft.client.gui.toasts.ToastGui;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.quest.QuestType;

import net.minecraft.client.gui.toasts.IToast.Visibility;

@OnlyIn(Dist.CLIENT)
public class NewQuestToast implements IToast {

    public static final ResourceLocation TEXTURE = new ResourceLocation(AdventureToolkit.MODID, "textures/gui/toasts.png");

    private final ITextComponent title;
    private final QuestType type;

    public NewQuestToast(ITextComponent title, QuestType type) {
        this.title = title;
        this.type = type;
    }

    @Override
    public Visibility render(MatrixStack matrixStack, ToastGui toastGui, long time) {
        toastGui.getMinecraft().getTextureManager().bind(TEXTURE);
        RenderSystem.color3f(1.0F, 1.0F, 1.0F);
        toastGui.blit(matrixStack, 0, 0, 0, this.type.getToastTextureV(), this.width(), this.height());
        toastGui.getMinecraft().font.draw(matrixStack, this.title, this.width() - toastGui.getMinecraft().font.width(this.title.getString()) - 8, 12.0F, 4210752);
        return time >= 5000L ? Visibility.HIDE : Visibility.SHOW;
    }
}
