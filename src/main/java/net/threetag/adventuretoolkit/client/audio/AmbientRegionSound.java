package net.threetag.adventuretoolkit.client.audio;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ITickableSound;
import net.minecraft.client.audio.LocatableSound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.MathHelper;
import net.threetag.adventuretoolkit.region.Region;
import net.threetag.adventuretoolkit.region.RegionManager;

public class AmbientRegionSound extends LocatableSound implements ITickableSound {

    private boolean donePlaying;
    private final Region region;
    private final float maxVolume;

    public AmbientRegionSound(Region region, ResourceLocation sound, float volume, float pitch, boolean repeat, int repeatDelay) {
        super(sound, SoundCategory.AMBIENT);
        this.region = region;
        this.volume = 0.1F;
        this.maxVolume = volume;
        this.pitch = pitch;
        this.x = Minecraft.getInstance().player.getX();
        this.y = Minecraft.getInstance().player.getY();
        this.z = Minecraft.getInstance().player.getZ();
        this.looping = repeat;
        this.delay = repeatDelay;
        this.relative = false;
    }

    @Override
    public void tick() {
        this.x = Minecraft.getInstance().player.getX();
        this.y = Minecraft.getInstance().player.getY();
        this.z = Minecraft.getInstance().player.getZ();

        if (RegionManager.Client.CURRENT_REGION != this.region) {
            this.volume -= 0.02F;
            if (this.volume <= 0)
                this.finishPlaying();
        } else {
            if (this.volume < maxVolume) {
                this.volume = MathHelper.clamp(this.volume + 0.02F, 0F, maxVolume);
            }
        }
    }

    @Override
    public boolean isStopped() {
        return this.donePlaying;
    }

    protected final void finishPlaying() {
        this.donePlaying = true;
        this.looping = false;
    }
}
