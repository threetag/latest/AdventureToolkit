package net.threetag.adventuretoolkit.client.renderer;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.region.Region;
import net.threetag.adventuretoolkit.region.RegionManager;
import net.threetag.adventuretoolkit.region.feature.HorizonImageFeature;
import net.threetag.adventuretoolkit.region.feature.IRegionFeature;
import net.threetag.adventuretoolkit.region.feature.ISkyRenderFeature;
import net.threetag.adventuretoolkit.region.feature.RegionFeatures;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Arrays;
import java.util.List;

@Mod.EventBusSubscriber(modid = AdventureToolkit.MODID, value = Dist.CLIENT)
public class SkyBoxRenderer {

    private static float red, green, blue;

    public static void preRender(MatrixStack matrixStackIn, float partialTicks, CallbackInfo callbackInfo) {
        Region region = RegionManager.Client.CURRENT_REGION;
        boolean found = false;
        if (region != null) {
            for (IRegionFeature feature : region.getFeaturesByType(RegionFeatures.SKY_BOX.get())) {
                if (feature instanceof ISkyRenderFeature) {
                    int red = 255, green = 255, blue = 255;
                    RenderSystem.color4f(1F, 1F, 1F, 1F);

                    if (feature instanceof HorizonImageFeature) {
                        Vector3d vector3d = Minecraft.getInstance().level.getSkyColor(Minecraft.getInstance().gameRenderer.getMainCamera().getBlockPosition(), partialTicks);
                        red = (int) (vector3d.x * 255F);
                        green = (int) (vector3d.y * 255F);
                        blue = (int) (vector3d.z * 255F);
                    }

                    int alpha = (int) (((ISkyRenderFeature) feature).getAlpha() * 255F);
                    List<Direction> directions = ((ISkyRenderFeature) feature).getDirections().isEmpty() ? Arrays.asList(Direction.values()) : ((ISkyRenderFeature) feature).getDirections();

                    RenderSystem.depthMask(false);
                    RenderSystem.enableTexture();

                    for (Direction direction : directions) {
                        matrixStackIn.pushPose();

                        if (direction == Direction.DOWN) {
                            // nothing
                        } else if (direction == Direction.UP) {
                            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));
                        } else if (direction == Direction.WEST) {
                            matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(90F));
                            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
                        } else if (direction == Direction.EAST) {
                            matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(-90F));
                            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
                        } else if (direction == Direction.NORTH) {
                            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
                        } else {
                            matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(180F));
                            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
                        }

                        float distance = Minecraft.getInstance().options.renderDistance * 16;
                        Minecraft.getInstance().getTextureManager().bind(((ISkyRenderFeature) feature).getTexture());
                        BufferBuilder bufferbuilder = Tessellator.getInstance().getBuilder();
                        bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
                        bufferbuilder.vertex(matrixStackIn.last().pose(), -distance, -distance, -distance).uv(0.0F, 0.0F).endVertex();
                        bufferbuilder.vertex(matrixStackIn.last().pose(), -distance, -distance, distance).uv(0.0F, 1.0F).endVertex();
                        bufferbuilder.vertex(matrixStackIn.last().pose(), distance, -distance, distance).uv(1.0F, 1.0F).endVertex();
                        bufferbuilder.vertex(matrixStackIn.last().pose(), distance, -distance, -distance).uv(1.0F, 0.0F).endVertex();
                        bufferbuilder.end();
                        WorldVertexBufferUploader.end(bufferbuilder);

                        matrixStackIn.popPose();
                    }

                    RenderSystem.depthMask(true);
                    found = true;
                }
            }
        }
        if (found) {
            callbackInfo.cancel();
        }
    }

    public static void postRender(MatrixStack matrixStackIn, float partialTicks) {
        Region region = RegionManager.Client.CURRENT_REGION;

        if (region != null) {
            for (IRegionFeature feature : region.getFeaturesByType(RegionFeatures.HORIZON_IMAGE.get())) {
                if (feature instanceof ISkyRenderFeature) {
                    int red = 255, green = 255, blue = 255;
                    RenderSystem.color4f(1F, 1F, 1F, 1F);

                    if (feature instanceof HorizonImageFeature) {
                        Vector3d vector3d = Minecraft.getInstance().level.getSkyColor(Minecraft.getInstance().gameRenderer.getMainCamera().getBlockPosition(), partialTicks);
                        red = (int) (vector3d.x * 255F);
                        green = (int) (vector3d.y * 255F);
                        blue = (int) (vector3d.z * 255F);
                    }

                    int alpha = (int) (((ISkyRenderFeature) feature).getAlpha() * 255F);
                    List<Direction> directions = ((ISkyRenderFeature) feature).getDirections().isEmpty() ? Arrays.asList(Direction.values()) : ((ISkyRenderFeature) feature).getDirections();

                    RenderSystem.depthMask(false);
                    RenderSystem.enableTexture();

                    for (Direction direction : directions) {
                        matrixStackIn.pushPose();

                        if (direction == Direction.DOWN) {
                            // nothing
                        } else if (direction == Direction.UP) {
                            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));
                        } else if (direction == Direction.WEST) {
                            matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(90F));
                            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
                        } else if (direction == Direction.EAST) {
                            matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(-90F));
                            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
                        } else if (direction == Direction.NORTH) {
                            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
                        } else {
                            matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(180F));
                            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90F));
                        }

                        IRenderTypeBuffer.Impl buffer = Minecraft.getInstance().renderBuffers().bufferSource();
                        IVertexBuilder vertexBuilder = buffer.getBuffer(RenderType.entityTranslucent(((ISkyRenderFeature) feature).getTexture()));
                        float distance = Minecraft.getInstance().options.renderDistance * 16;

                        SkyBoxRenderer.drawVertex(matrixStackIn, vertexBuilder, -distance, -distance, -distance, 0.0F, 0.0F, 0, 1, 0, 0, red, green, blue, alpha);
                        SkyBoxRenderer.drawVertex(matrixStackIn, vertexBuilder, -distance, -distance, distance, 0.0F, 1.0F, 0, 1, 0, 0, red, green, blue, alpha);
                        SkyBoxRenderer.drawVertex(matrixStackIn, vertexBuilder, distance, -distance, distance, 1.0F, 1.0F, 0, 1, 0, 0, red, green, blue, alpha);
                        SkyBoxRenderer.drawVertex(matrixStackIn, vertexBuilder, distance, -distance, -distance, 1.0F, 0.0F, 0, 1, 0, 0, red, green, blue, alpha);

                        matrixStackIn.popPose();
                    }

                    RenderSystem.depthMask(true);
                }
            }
        }
    }

    public static void drawVertex(MatrixStack matrixStack, IVertexBuilder vertexBuilder, float offsetX, float offsetY, float offsetZ, float textureX, float textureY, int normalX, int normalY, int normalZ, int packedLightIn, int red, int green, int blue, int alpha) {
        vertexBuilder.vertex(matrixStack.last().pose(), offsetX, offsetY, offsetZ).color(red, green, blue, alpha).uv(textureX, textureY).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(packedLightIn).normal(matrixStack.last().normal(), (float) normalX, (float) normalZ, (float) normalY).endVertex();
    }

    @SubscribeEvent
    public static void fogColor(EntityViewRenderEvent.FogColors e) {
        red = e.getRed();
        blue = e.getBlue();
        green = e.getGreen();
    }

}
