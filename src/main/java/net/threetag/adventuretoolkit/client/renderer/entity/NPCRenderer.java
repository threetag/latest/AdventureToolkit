package net.threetag.adventuretoolkit.client.renderer.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.entity.*;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.threetag.adventuretoolkit.entity.NPCEntity;
import net.threetag.adventuretoolkit.util.ImageUtil;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Swirtzly
 * on 22/03/2020 @ 15:18
 */
public class NPCRenderer extends BipedRenderer<net.threetag.adventuretoolkit.entity.NPCEntity, PlayerModel<NPCEntity>> {

    public static HashMap<Entity, ResourceLocation> TEXTURES = new HashMap<>();

    private static final PlayerModel<NPCEntity> ALEX = new PlayerModel<>(0.0F, true);
    private static final PlayerModel<NPCEntity> STEVE = new PlayerModel<>(0.0F, false);

    public NPCRenderer(EntityRendererManager rendererManager) {
        super(rendererManager, STEVE, 0.5F);
    }

    @Override
    protected void scale(NPCEntity entity, MatrixStack matrixStack, float p_225620_3_) {
        this.model = entity.isAlex() ? ALEX : STEVE;
        super.scale(entity, matrixStack, p_225620_3_);
    }

    @Override
    public ResourceLocation getTextureLocation(NPCEntity entity) {
        return getOrCreateTexture(entity);
    }

    public static ResourceLocation getOrCreateTexture(NPCEntity npc) {
        if (TEXTURES.containsKey(npc)) {
            return TEXTURES.get(npc);
        } else {
            ((Runnable) () -> {
                try {
                    TEXTURES.put(npc, ImageUtil.createTexture(new URL("https://i.imgur.com/TZToTcE.png")));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).run();
        }
        return new ResourceLocation("textures/entity/alex.png");
    }


}
