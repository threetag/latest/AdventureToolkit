package net.threetag.adventuretoolkit.client.renderer;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.EntityTickableSound;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.InputUpdateEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.threecore.util.MathUtil;

import static net.minecraft.client.gui.AbstractGui.fill;

@OnlyIn(Dist.CLIENT)
@Mod.EventBusSubscriber(modid = AdventureToolkit.MODID, value = Dist.CLIENT)
public class TeleportTransitionRenderer {

    public static int duration;
    private static int progress;
    private static int prevProgress;

    public static void start(SoundEvent soundEvent, int length) {
        TeleportTransitionRenderer.duration = length;
        Minecraft.getInstance().getSoundManager().play(new EntityTickableSound(soundEvent, SoundCategory.AMBIENT, Minecraft.getInstance().player));
    }

    @SubscribeEvent
    public static void renderHUD(RenderGameOverlayEvent.Post e) {
        if (e.getType() == RenderGameOverlayEvent.ElementType.HELMET && Minecraft.getInstance().options.getCameraType().isFirstPerson() && duration > 0) {
            float opacity = MathHelper.clamp(MathHelper.sin((float) ((MathUtil.interpolate(prevProgress, progress, e.getPartialTicks()) / duration) * Math.PI)) * 1.3F, 0F, 1F);

            if (opacity > 0F) {
                RenderSystem.disableDepthTest();
                RenderSystem.disableAlphaTest();
                int width = Minecraft.getInstance().getWindow().getGuiScaledWidth();
                int height = Minecraft.getInstance().getWindow().getGuiScaledHeight();
                int color = (int) (255F * opacity) << 24;
                fill(e.getMatrixStack(), 0, 0, width, height, color);
                RenderSystem.enableAlphaTest();
                RenderSystem.enableDepthTest();

            }
        }
    }

    @SubscribeEvent
    public static void onClientTick(TickEvent.ClientTickEvent e) {
        if (e.phase == TickEvent.Phase.END && Minecraft.getInstance() != null && Minecraft.getInstance().player != null) {
            prevProgress = progress;

            if (!Minecraft.getInstance().isPaused()) {
                if (progress < duration) {
                    progress++;
                } else {
                    progress = 0;
                    duration = 0;
                }
            }
        }
    }

    @SubscribeEvent
    public static void onInputUpdate(InputUpdateEvent e) {
        if (progress > 0) {
            e.getMovementInput().right = false;
            e.getMovementInput().left = false;
            e.getMovementInput().up = false;
            e.getMovementInput().down = false;
            e.getMovementInput().shiftKeyDown = false;
            e.getMovementInput().jumping = false;
        }
    }

}
