package net.threetag.adventuretoolkit;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.AddReloadListenerEvent;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.threetag.adventuretoolkit.block.ATBlocks;
import net.threetag.adventuretoolkit.capability.ATCapabilities;
import net.threetag.adventuretoolkit.command.MoneyCommand;
import net.threetag.adventuretoolkit.command.TransitionalTeleportCommand;
import net.threetag.adventuretoolkit.data.ATLangProviders;
import net.threetag.adventuretoolkit.entity.ATEntityTypes;
import net.threetag.adventuretoolkit.money.DefaultMoneyHandler;
import net.threetag.adventuretoolkit.network.*;
import net.threetag.adventuretoolkit.particle.ATParticleTypes;
import net.threetag.adventuretoolkit.quest.QuestManager;
import net.threetag.adventuretoolkit.quest.requirement.QuestRequirements;
import net.threetag.adventuretoolkit.region.RegionManager;
import net.threetag.adventuretoolkit.region.feature.RegionFeatures;
import net.threetag.adventuretoolkit.script.accessor.QuestHandlerAccessor;
import net.threetag.adventuretoolkit.script.binding.ATScriptBinding;
import net.threetag.adventuretoolkit.sequence.SequenceManager;
import net.threetag.threecore.scripts.ScriptManager;
import net.threetag.threecore.scripts.accessors.ScriptAccessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Mod(AdventureToolkit.MODID)
public class AdventureToolkit {

    public static final String MODID = "adventuretoolkit";
    public static final Logger LOGGER = LogManager.getLogger();
    public static SimpleChannel NETWORK_CHANNEL;
    private static int networkId = -1;

    public AdventureToolkit() {
        FMLJavaModLoadingContext.get().getModEventBus().register(this);
        MinecraftForge.EVENT_BUS.register(new Events());
        MinecraftForge.EVENT_BUS.addListener(RegionManager::onWorldJoin);
        MinecraftForge.EVENT_BUS.addListener(QuestManager::onWorldJoin);
        MinecraftForge.EVENT_BUS.addListener(DefaultMoneyHandler::onRegisterThreeData);

        ATBlocks.BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ATParticleTypes.PARTICLE_TYPES.register(FMLJavaModLoadingContext.get().getModEventBus());
        RegionFeatures.REGION_FEATURES.register(FMLJavaModLoadingContext.get().getModEventBus());
        QuestRequirements.QUEST_REQUIREMENT_TYPES.register(FMLJavaModLoadingContext.get().getModEventBus());

        registerMessage(StartTeleportTransitionMessage.class, StartTeleportTransitionMessage::toBytes, StartTeleportTransitionMessage::new, StartTeleportTransitionMessage::handle);
        registerMessage(SyncRegionsMessage.class, SyncRegionsMessage::toBytes, SyncRegionsMessage::new, SyncRegionsMessage::handle);
        registerMessage(SyncQuestsMessage.class, SyncQuestsMessage::toBytes, SyncQuestsMessage::new, SyncQuestsMessage::handle);
        registerMessage(SyncPlayerQuestsMessage.class, SyncPlayerQuestsMessage::toBytes, SyncPlayerQuestsMessage::new, SyncPlayerQuestsMessage::handle);
        registerMessage(SyncQuestProgressMessage.class, SyncQuestProgressMessage::toBytes, SyncQuestProgressMessage::new, SyncQuestProgressMessage::handle);
        registerMessage(StartDialogueMessage.class, StartDialogueMessage::toBytes, StartDialogueMessage::new, StartDialogueMessage::handle);
        registerMessage(StopSequenceMessage.class, StopSequenceMessage::toBytes, StopSequenceMessage::new, StopSequenceMessage::handle);
        registerMessage(NewQuestMessage.class, NewQuestMessage::toBytes, NewQuestMessage::new, NewQuestMessage::handle);

        // Adding acript accessor classes for documentation
        ScriptAccessor.accessorClasses.add(QuestHandlerAccessor.class);
        ScriptManager.registerBinding("adventureToolkit", ATScriptBinding::new);
    }

    @SubscribeEvent
    public void setup(FMLCommonSetupEvent e) {
        ATCapabilities.init();
    }

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public void setup(final FMLClientSetupEvent event) {
        ATParticleTypes.registerFactories();
        MinecraftForge.EVENT_BUS.addListener(RegionFeatures::generateDocumentation);
    }

    @SubscribeEvent
    public void gatherData(GatherDataEvent e) {
        e.getGenerator().addProvider(new ATLangProviders.English(e.getGenerator()));
        e.getGenerator().addProvider(new ATLangProviders.Norwegian(e.getGenerator()));
        e.getGenerator().addProvider(new ATLangProviders.German(e.getGenerator()));
    }

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public void setupClient(final FMLClientSetupEvent event) {
        ATEntityTypes.initRenderers();
    }

    public static <MSG> int registerMessage(Class<MSG> messageType, BiConsumer<MSG, PacketBuffer> encoder, Function<PacketBuffer, MSG> decoder, BiConsumer<MSG, Supplier<NetworkEvent.Context>> messageConsumer) {
        if (NETWORK_CHANNEL == null)
            NETWORK_CHANNEL = NetworkRegistry.newSimpleChannel(new ResourceLocation(AdventureToolkit.MODID, AdventureToolkit.MODID), () -> "1.0", (s) -> true, (s) -> true);

        int id = networkId++;
        NETWORK_CHANNEL.registerMessage(id, messageType, encoder, decoder, messageConsumer);
        return id;
    }

    public static class Events {

        @SubscribeEvent
        public void serverStarting(RegisterCommandsEvent e) {
            TransitionalTeleportCommand.register(e.getDispatcher());
            MoneyCommand.register(e.getDispatcher());
        }

        @SubscribeEvent
        public void addListenerEvent(AddReloadListenerEvent event) {
            event.addListener(new RegionManager());
            event.addListener(new SequenceManager());
            event.addListener(QuestManager.INSTANCE = new QuestManager());
        }

    }
}
