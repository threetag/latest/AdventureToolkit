package net.threetag.adventuretoolkit.quest.requirement;

import com.google.gson.JsonObject;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;

import javax.annotation.Nullable;

public abstract class QuestRequirementType extends ForgeRegistryEntry<QuestRequirementType> {

    public abstract AbstractQuestRequirement read(JsonObject json);

    @Nullable
    public abstract AbstractQuestRequirement read(PacketBuffer buffer);

    public abstract void write(PacketBuffer buffer, AbstractQuestRequirement requirement);

    @Nullable
    public static ITextComponent readCustomTitle(JsonObject json) {
        return JSONUtils.isValidNode(json, "custom_title") ? ITextComponent.Serializer.fromJson(JSONUtils.getAsJsonObject(json, "custom_title").toString()) : null;
    }

    @Nullable
    public static ITextComponent readCustomTitle(PacketBuffer buffer) {
        if (buffer.readBoolean()) {
            return buffer.readComponent();
        }
        return null;
    }

    public static void writeCustomTitle(PacketBuffer buffer, @Nullable ITextComponent customTitle) {
        buffer.writeBoolean(customTitle != null);
        if (customTitle != null) {
            buffer.writeComponent(customTitle);
        }
    }
}
