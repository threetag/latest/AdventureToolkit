package net.threetag.adventuretoolkit.quest.requirement;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.adventuretoolkit.quest.requirement.progress.AbstractQuestRequirementProgress;
import net.threetag.adventuretoolkit.quest.requirement.progress.IntegerRequirementProgress;

import javax.annotation.Nullable;

public class BreakBlocksRequirementType extends QuestRequirementType {

    @Override
    public AbstractQuestRequirement read(JsonObject json) {
        Block block = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(JSONUtils.getAsString(json, "block")));
        if (block == null) {
            throw new JsonParseException("Block " + JSONUtils.getAsString(json, "block") + " does not exist!");
        }
        return new BreakBlocksRequirement(block, JSONUtils.getAsInt(json, "amount"), readCustomTitle(json));
    }

    @Nullable
    @Override
    public AbstractQuestRequirement read(PacketBuffer buffer) {
        Block block = buffer.readRegistryIdSafe(Block.class);
        int amount = buffer.readInt();
        return new BreakBlocksRequirement(block, amount, readCustomTitle(buffer));
    }

    @Override
    public void write(PacketBuffer buffer, AbstractQuestRequirement requirement) {
        buffer.writeRegistryId(((BreakBlocksRequirement) requirement).block);
        buffer.writeInt(((BreakBlocksRequirement) requirement).amount);
        writeCustomTitle(buffer, requirement.customTitle);
    }

    public static class BreakBlocksRequirement extends AbstractQuestRequirement {

        public final Block block;
        public final int amount;

        public BreakBlocksRequirement(Block block, int amount, ITextComponent customTitle) {
            super(QuestRequirements.BREAK_BLOCKS.get(), customTitle);
            this.block = block;
            this.amount = amount;
        }

        @Override
        public AbstractQuestRequirementProgress createProgress() {
            return new IntegerRequirementProgress(this.amount);
        }

        @Override
        public ITextComponent getDefaultTitle() {
            ITextComponent blockName = this.block.asItem().getName(new ItemStack(this.block));
            return new TranslationTextComponent("quest.requirement." + this.getType().getRegistryName().getNamespace() + "." + this.getType().getRegistryName().getPath() + ".title", amount, blockName);
        }
    }
}
