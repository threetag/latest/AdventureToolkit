package net.threetag.adventuretoolkit.quest.requirement.progress;

import com.mojang.datafixers.util.Pair;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;

public abstract class AbstractQuestRequirementProgress implements INBTSerializable<CompoundNBT> {

    private boolean dirty;

    public abstract void reset();

    public abstract boolean isDone();

    public abstract Pair<Integer, Integer> getProgress();

    public boolean isDirty() {
        return dirty;
    }

    public void markDirty() {
        this.markDirty(true);
    }

    public void markDirty(boolean dirty) {
        this.dirty = dirty;
    }
}
