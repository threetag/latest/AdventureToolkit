package net.threetag.adventuretoolkit.quest.requirement.progress;

import com.mojang.datafixers.util.Pair;
import net.minecraft.nbt.CompoundNBT;

public class BooleanRequirementProgress extends AbstractQuestRequirementProgress {

    private boolean done;

    @Override
    public void reset() {
        if (this.done) {
            this.done = false;
            this.markDirty();
        }
    }

    @Override
    public boolean isDone() {
        return this.done;
    }

    public void markDone() {
        if (!this.done) {
            this.done = true;
            this.markDirty();
        }
    }

    @Override
    public Pair<Integer, Integer> getProgress() {
        return Pair.of(this.done ? 1 : 0, 1);
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT nbt = new CompoundNBT();
        nbt.putBoolean("Done", this.done);
        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.done = nbt.getBoolean("Done");
    }
}
