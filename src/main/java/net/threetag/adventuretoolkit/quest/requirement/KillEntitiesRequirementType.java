package net.threetag.adventuretoolkit.quest.requirement;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.minecraft.entity.EntityType;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.adventuretoolkit.quest.requirement.progress.AbstractQuestRequirementProgress;
import net.threetag.adventuretoolkit.quest.requirement.progress.IntegerRequirementProgress;

import javax.annotation.Nullable;

public class KillEntitiesRequirementType extends QuestRequirementType {

    @Override
    public AbstractQuestRequirement read(JsonObject json) {
        EntityType<?> entityType = ForgeRegistries.ENTITIES.getValue(new ResourceLocation(JSONUtils.getAsString(json, "entity_type")));
        if (entityType == null) {
            throw new JsonParseException("Entity type " + JSONUtils.getAsString(json, "entity_type") + " does not exist!");
        }
        return new KillEntitiesRequirement(entityType, JSONUtils.getAsInt(json, "amount"), readCustomTitle(json));
    }

    @Nullable
    @Override
    public AbstractQuestRequirement read(PacketBuffer buffer) {
        EntityType<?> block = buffer.readRegistryIdSafe(EntityType.class);
        int amount = buffer.readInt();
        return new KillEntitiesRequirement(block, amount, readCustomTitle(buffer));
    }

    @Override
    public void write(PacketBuffer buffer, AbstractQuestRequirement requirement) {
        buffer.writeRegistryId(((KillEntitiesRequirement) requirement).entityType);
        buffer.writeInt(((KillEntitiesRequirement) requirement).amount);
        writeCustomTitle(buffer, requirement.customTitle);
    }

    public static class KillEntitiesRequirement extends AbstractQuestRequirement {

        public final EntityType<?> entityType;
        public final int amount;

        public KillEntitiesRequirement(EntityType<?> entityType, int amount, ITextComponent customTitle) {
            super(QuestRequirements.KILL_ENTITIES.get(), customTitle);
            this.entityType = entityType;
            this.amount = amount;
        }

        @Override
        public AbstractQuestRequirementProgress createProgress() {
            return new IntegerRequirementProgress(this.amount);
        }

        @Override
        public ITextComponent getDefaultTitle() {
            return new TranslationTextComponent("quest.requirement." + this.getType().getRegistryName().getNamespace() + "." + this.getType().getRegistryName().getPath() + ".title", amount, this.entityType.getDescription());
        }
    }
}
