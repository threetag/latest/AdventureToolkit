package net.threetag.adventuretoolkit.quest.requirement;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.threetag.adventuretoolkit.AdventureToolkit;

import java.util.function.Supplier;

public class QuestRequirements {

    public static final DeferredRegister<QuestRequirementType> QUEST_REQUIREMENT_TYPES = DeferredRegister.create(QuestRequirementType.class, AdventureToolkit.MODID);
    @SuppressWarnings("unchecked")
    public static final Supplier<IForgeRegistry<QuestRequirementType>> REGISTRY = QUEST_REQUIREMENT_TYPES.makeRegistry("quest_requirement_types", () -> new RegistryBuilder().setIDRange(0, 512).setType(QuestRequirementType.class));

    public static final RegistryObject<QuestRequirementType> BREAK_BLOCKS = QUEST_REQUIREMENT_TYPES.register("break_blocks", BreakBlocksRequirementType::new);
    public static final RegistryObject<QuestRequirementType> KILL_ENTITIES = QUEST_REQUIREMENT_TYPES.register("kill_entities", KillEntitiesRequirementType::new);

}
