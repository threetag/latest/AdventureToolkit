package net.threetag.adventuretoolkit.quest.requirement;

import net.minecraft.util.text.ITextComponent;
import net.threetag.adventuretoolkit.quest.requirement.progress.AbstractQuestRequirementProgress;

public abstract class AbstractQuestRequirement {

    private final QuestRequirementType type;
    public final ITextComponent customTitle;

    public AbstractQuestRequirement(QuestRequirementType type, ITextComponent customTitle) {
        this.type = type;
        this.customTitle = customTitle;
    }

    public QuestRequirementType getType() {
        return type;
    }

    public abstract AbstractQuestRequirementProgress createProgress();

    public abstract ITextComponent getDefaultTitle();

    public ITextComponent getTitle() {
        return this.customTitle != null ? this.customTitle : this.getDefaultTitle();
    }
}
