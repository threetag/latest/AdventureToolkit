package net.threetag.adventuretoolkit.quest.requirement.progress;

import com.mojang.datafixers.util.Pair;
import net.minecraft.nbt.CompoundNBT;

public class IntegerRequirementProgress extends AbstractQuestRequirementProgress {

    private int i;
    private final int max;

    public IntegerRequirementProgress(int max) {
        this.max = max;
    }

    @Override
    public void reset() {
        this.i = 0;
        this.markDirty();
    }

    @Override
    public boolean isDone() {
        return this.i >= this.max;
    }

    public void incr() {
        if (!this.isDone()) {
            this.i++;
            this.markDirty();
        }
    }

    @Override
    public Pair<Integer, Integer> getProgress() {
        return Pair.of(this.i, this.max);
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT nbt = new CompoundNBT();
        nbt.putInt("Progress", this.i);
        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.i = nbt.getInt("Progress");
    }
}
