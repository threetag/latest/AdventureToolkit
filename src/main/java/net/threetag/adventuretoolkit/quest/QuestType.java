package net.threetag.adventuretoolkit.quest;

public enum QuestType {

    STORY("story", 0, 0),
    SIDE("side", 27, 32),
    DAILY("daily", 54, 64);

    private final String name;
    private final int buttonTextureV;
    private final int toastTextureV;

    QuestType(String name, int buttonTextureV, int toastTextureV) {
        this.name = name;
        this.buttonTextureV = buttonTextureV;
        this.toastTextureV = toastTextureV;
    }

    public String getName() {
        return this.name;
    }

    public int getButtonTextureV() {
        return buttonTextureV;
    }

    public int getToastTextureV() {
        return toastTextureV;
    }

    public static QuestType byName(String name) {
        for (QuestType type : values()) {
            if (type.name.equals(name)) {
                return type;
            }
        }
        return null;
    }
}
