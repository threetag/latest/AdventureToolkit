package net.threetag.adventuretoolkit.quest;

import net.minecraft.entity.player.PlayerEntity;

import java.util.List;

public interface IQuestHolder {

    void tick(PlayerEntity player);

    boolean startQuest(Quest quest, PlayerEntity player);

    List<QuestInstance> getActiveQuests();

    List<Quest> getCompletedQuests();

    void clearQuests();

    void reload();
}
