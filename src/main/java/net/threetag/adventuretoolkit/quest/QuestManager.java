package net.threetag.adventuretoolkit.quest;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.mojang.datafixers.util.Pair;
import net.minecraft.client.resources.JsonReloadListener;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.thread.EffectiveSide;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.capability.QuestHolderCapability;
import net.threetag.adventuretoolkit.network.SyncPlayerQuestsMessage;
import net.threetag.adventuretoolkit.network.SyncQuestsMessage;
import net.threetag.adventuretoolkit.quest.requirement.AbstractQuestRequirement;
import net.threetag.adventuretoolkit.quest.requirement.QuestRequirementType;
import net.threetag.adventuretoolkit.quest.requirement.progress.AbstractQuestRequirementProgress;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class QuestManager extends JsonReloadListener {

    private static Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    public static QuestManager INSTANCE;
    private final Map<ResourceLocation, Quest> registeredQuests = Maps.newHashMap();

    public QuestManager() {
        super(GSON, "quests");
    }

    @Override
    protected void apply(Map<ResourceLocation, JsonElement> objectIn, IResourceManager resourceManagerIn, IProfiler profilerIn) {
        this.registeredQuests.clear();
        for (Map.Entry<ResourceLocation, JsonElement> entry : objectIn.entrySet()) {
            ResourceLocation resourcelocation = entry.getKey();
            try {
                Quest quest = new Quest(resourcelocation, entry.getValue().getAsJsonObject());
                this.registeredQuests.put(resourcelocation, quest);
            } catch (Exception e) {
                AdventureToolkit.LOGGER.error("Parsing error loading quest {}", resourcelocation, e);
            }
        }
        AdventureToolkit.LOGGER.info("Loaded {} quests", this.registeredQuests.size());

        if (ServerLifecycleHooks.getCurrentServer() != null) {
            for (ServerPlayerEntity player : ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayers()) {
                AdventureToolkit.NETWORK_CHANNEL.sendTo(new SyncQuestsMessage(getInstance().registeredQuests), player.connection.connection, NetworkDirection.PLAY_TO_CLIENT);
                player.getCapability(QuestHolderCapability.QUEST_HOLDER).ifPresent(questHolder -> {
                    questHolder.reload();
                    if (questHolder instanceof INBTSerializable<?>) {
                        AdventureToolkit.NETWORK_CHANNEL.sendTo(new SyncPlayerQuestsMessage(((INBTSerializable<CompoundNBT>) questHolder).serializeNBT()), player.connection.connection, NetworkDirection.PLAY_TO_CLIENT);
                    }
                });
            }
        }
    }

    public static void onWorldJoin(EntityJoinWorldEvent e) {
        if (e.getEntity() instanceof ServerPlayerEntity) {
            AdventureToolkit.NETWORK_CHANNEL.sendTo(new SyncQuestsMessage(getInstance().registeredQuests), ((ServerPlayerEntity) e.getEntity()).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
            e.getEntity().getCapability(QuestHolderCapability.QUEST_HOLDER).ifPresent(questHolder -> {
                if (questHolder instanceof INBTSerializable<?>) {
                    AdventureToolkit.NETWORK_CHANNEL.sendTo(new SyncPlayerQuestsMessage(((INBTSerializable<CompoundNBT>) questHolder).serializeNBT()), ((ServerPlayerEntity) e.getEntity()).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
                }
            });
        }
    }

    public static QuestManager getInstance() {
        return EffectiveSide.get() == LogicalSide.SERVER ? INSTANCE : getClientManager();
    }

    @OnlyIn(Dist.CLIENT)
    private static QuestManager getClientManager() {
        return new Client();
    }

    public Quest getQuestById(ResourceLocation id) {
        return this.registeredQuests.get(id);
    }

    public Collection<Quest> getQuests() {
        return this.registeredQuests.values();
    }

    public static List<Pair<AbstractQuestRequirement, AbstractQuestRequirementProgress>> getRequirements(PlayerEntity playerEntity, QuestRequirementType type) {
        List<Pair<AbstractQuestRequirement, AbstractQuestRequirementProgress>> requirements = new ArrayList<>();
        playerEntity.getCapability(QuestHolderCapability.QUEST_HOLDER).ifPresent(questHolder -> {
            for (QuestInstance instance : questHolder.getActiveQuests()) {
                for (Map.Entry<String, AbstractQuestRequirement> entry : instance.getQuest().getRequirements().entrySet()) {
                    if (entry.getValue().getType() == type) {
                        requirements.add(Pair.of(entry.getValue(), instance.getProgress(entry.getKey())));
                    }
                }
            }
        });
        return requirements;
    }

    @OnlyIn(Dist.CLIENT)
    public static class Client extends QuestManager {

        public static final Map<ResourceLocation, Quest> QUESTS = Maps.newHashMap();

        public Client() {

        }

        @Override
        public Collection<Quest> getQuests() {
            return QUESTS.values();
        }

        @Override
        public Quest getQuestById(ResourceLocation id) {
            return QUESTS.get(id);
        }
    }
}
