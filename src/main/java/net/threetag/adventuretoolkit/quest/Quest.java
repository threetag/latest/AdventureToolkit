package net.threetag.adventuretoolkit.quest;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.threetag.adventuretoolkit.action.IAction;
import net.threetag.adventuretoolkit.quest.requirement.AbstractQuestRequirement;
import net.threetag.adventuretoolkit.quest.requirement.QuestRequirementType;
import net.threetag.adventuretoolkit.quest.requirement.QuestRequirements;
import net.threetag.adventuretoolkit.action.Actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Quest {

    private final ResourceLocation id;
    private final ITextComponent title;
    private final ITextComponent description;
    private final QuestType type;
    private final Map<String, AbstractQuestRequirement> requirements = new HashMap<>();
    private final List<IAction> rewards = new ArrayList<>();

    public Quest(ResourceLocation id, JsonObject json) {
        this.id = id;
        this.title = ITextComponent.Serializer.fromJson(JSONUtils.getAsJsonObject(json, "title").toString());
        this.description = ITextComponent.Serializer.fromJson(JSONUtils.getAsJsonObject(json, "description").toString());
        this.type = QuestType.byName(JSONUtils.getAsString(json, "type", "side"));

        JSONUtils.getAsJsonObject(json, "requirements").entrySet().forEach((entry) -> {
            JsonObject requirementJson = entry.getValue().getAsJsonObject();
            QuestRequirementType type = QuestRequirements.REGISTRY.get().getValue(new ResourceLocation(JSONUtils.getAsString(requirementJson, "type")));
            if (type == null) {
                throw new JsonParseException("Quest requirement type '" + JSONUtils.getAsString(requirementJson, "type") + "' does not exist!");
            }
            this.requirements.put(entry.getKey(), type.read(requirementJson));
        });

        if (JSONUtils.isValidNode(json, "rewards")) {
            JsonArray rewards = JSONUtils.getAsJsonArray(json, "rewards");

            for (int i = 0; i < rewards.size(); i++) {
                JsonObject jsonObject = rewards.get(i).getAsJsonObject();
                IAction reward = Actions.parse(jsonObject);

                if (reward != null) {
                    this.rewards.add(reward);
                }
            }
        }
    }

    public Quest(ResourceLocation id, PacketBuffer buf) {
        this.id = id;
        this.title = buf.readComponent();
        this.description = buf.readComponent();
        this.type = QuestType.byName(buf.readUtf());
        int size = buf.readInt();
        for (int i = 0; i < size; i++) {
            String key = buf.readUtf(32767);
            QuestRequirementType type = buf.readRegistryIdSafe(QuestRequirementType.class);
            AbstractQuestRequirement requirement = type.read(buf);
            this.requirements.put(key, requirement);
        }
    }

    public void write(PacketBuffer buf) {
        buf.writeComponent(this.title);
        buf.writeComponent(this.description);
        buf.writeUtf(this.type.getName());
        buf.writeInt(this.requirements.size());
        this.requirements.forEach((key, requirement) -> {
            buf.writeUtf(key);
            buf.writeRegistryId(requirement.getType());
            requirement.getType().write(buf, requirement);
        });
    }

    public ResourceLocation getId() {
        return id;
    }

    public ITextComponent getTitle() {
        return title;
    }

    public ITextComponent getDescription() {
        return description;
    }

    public QuestType getType() {
        return type;
    }

    public Map<String, AbstractQuestRequirement> getRequirements() {
        return requirements;
    }

    public AbstractQuestRequirement getRequirement(String key) {
        return this.requirements.get(key);
    }

    public List<IAction> getRewards() {
        return rewards;
    }
}
