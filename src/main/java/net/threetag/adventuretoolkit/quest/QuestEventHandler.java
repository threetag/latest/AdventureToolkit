package net.threetag.adventuretoolkit.quest;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.CreativeScreen;
import net.minecraft.client.gui.screen.inventory.InventoryScreen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.capability.QuestHolderCapability;
import net.threetag.adventuretoolkit.capability.SequenceHolderCapability;
import net.threetag.adventuretoolkit.client.screen.ActiveQuestsScreen;
import net.threetag.adventuretoolkit.quest.requirement.BreakBlocksRequirementType;
import net.threetag.adventuretoolkit.quest.requirement.KillEntitiesRequirementType;
import net.threetag.adventuretoolkit.quest.requirement.QuestRequirements;
import net.threetag.adventuretoolkit.quest.requirement.progress.IntegerRequirementProgress;
import net.threetag.adventuretoolkit.sequence.AbstractSequence;
import net.threetag.adventuretoolkit.sequence.SequenceManager;

@Mod.EventBusSubscriber(modid = AdventureToolkit.MODID)
public class QuestEventHandler {

    @SubscribeEvent
    public static void onPlayerTick(TickEvent.PlayerTickEvent e) {
        if (e.phase == TickEvent.Phase.END && !e.player.level.isClientSide) {
            e.player.getCapability(QuestHolderCapability.QUEST_HOLDER).ifPresent(questHolder -> questHolder.tick(e.player));
        }
    }

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public static void onGuiInit(GuiScreenEvent.InitGuiEvent e) {
        if (e.getGui() instanceof InventoryScreen || e.getGui() instanceof CreativeScreen) {
            e.addWidget(new Button(0, 0, 60, 20, new StringTextComponent("quests"), (button) -> {
                Minecraft.getInstance().setScreen(new ActiveQuestsScreen());
            }));
        }
    }

    @SubscribeEvent
    public static void onItemRightClick(PlayerInteractEvent.RightClickItem e) {
        if (e.getItemStack().getItem() == Items.STICK) {
//            e.getPlayer().getCapability(QuestHolderCapability.QUEST_HOLDER).ifPresent(questHolder -> {
//                questHolder.clearQuests();
//                for(Quest quest : QuestManager.getInstance().getQuests()) {
//                    questHolder.startQuest(quest);
//                }
//            });
            AbstractSequence sequence = SequenceManager.getInstance().getSequence(new ResourceLocation("test:test_sequence"));

            if(sequence != null && e.getPlayer() instanceof ServerPlayerEntity) {
                e.getPlayer().getCapability(SequenceHolderCapability.SEQUENCE_HOLDER).ifPresent(sequenceHolder -> {
                    sequenceHolder.startSequence(sequence, (ServerPlayerEntity) e.getPlayer());
                });
            }
        }
    }

    @SubscribeEvent
    public static void onBlockBreak(BlockEvent.BreakEvent e) {
        if (!e.getPlayer().level.isClientSide) {
            QuestManager.getRequirements(e.getPlayer(), QuestRequirements.BREAK_BLOCKS.get()).forEach(pair -> {
                BreakBlocksRequirementType.BreakBlocksRequirement requirement = (BreakBlocksRequirementType.BreakBlocksRequirement) pair.getFirst();
                if (requirement.block == e.getState().getBlock() && pair.getSecond() instanceof IntegerRequirementProgress) {
                    ((IntegerRequirementProgress) pair.getSecond()).incr();
                }
            });
        }
    }

    @SubscribeEvent
    public static void onLivingDeath(LivingDeathEvent e) {
        if (!e.getEntity().level.isClientSide && e.getSource().getEntity() instanceof PlayerEntity) {
            QuestManager.getRequirements((PlayerEntity) e.getSource().getEntity(), QuestRequirements.KILL_ENTITIES.get()).forEach(pair -> {
                KillEntitiesRequirementType.KillEntitiesRequirement requirement = (KillEntitiesRequirementType.KillEntitiesRequirement) pair.getFirst();
                if (requirement.entityType == e.getEntity().getType() && pair.getSecond() instanceof IntegerRequirementProgress) {
                    ((IntegerRequirementProgress) pair.getSecond()).incr();
                }
            });
        }
    }

}
