package net.threetag.adventuretoolkit.quest;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;
import net.threetag.adventuretoolkit.quest.requirement.progress.AbstractQuestRequirementProgress;

import java.util.HashMap;
import java.util.Map;

public class QuestInstance implements INBTSerializable<CompoundNBT> {

    private Quest quest;
    private final Map<String, AbstractQuestRequirementProgress> progresses = new HashMap<>();

    public QuestInstance(Quest quest) {
        this.quest = quest;
        this.quest.getRequirements().forEach((key, requirement) -> {
            this.progresses.put(key, requirement.createProgress());
        });
    }

    public QuestInstance(CompoundNBT nbt) {
        this.deserializeNBT(nbt);
    }

    public Quest getQuest() {
        return quest;
    }

    public AbstractQuestRequirementProgress getProgress(String key) {
        return this.progresses.get(key);
    }

    public Map<String, AbstractQuestRequirementProgress> getProgresses() {
        return progresses;
    }

    public float getTotalProgress() {
        if(this.progresses.isEmpty()) {
            return 0F;
        }

        float f = 0F;
        float max = 0F;
        for (AbstractQuestRequirementProgress progress : this.progresses.values()) {
            f += progress.getProgress().getFirst();
            max += progress.getProgress().getSecond();
        }
        return f / max;
    }

    public boolean isObsolete() {
        return this.quest == null;
    }

    public void reload() {
        if(this.quest != null) {
            this.quest = QuestManager.getInstance().getQuestById(this.quest.getId());
        }
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT nbt = new CompoundNBT();
        nbt.putString("Quest", this.quest.getId().toString());
        this.progresses.forEach((key, progress) -> nbt.put(key, progress.serializeNBT()));
        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.quest = QuestManager.getInstance().getQuestById(new ResourceLocation(nbt.getString("Quest")));
        if(this.quest != null) {
            this.quest.getRequirements().forEach((key, requirement) -> {
                this.progresses.put(key, requirement.createProgress());
            });
            this.progresses.forEach((key, progress) -> progress.deserializeNBT(nbt.getCompound(key)));
        }
    }
}
