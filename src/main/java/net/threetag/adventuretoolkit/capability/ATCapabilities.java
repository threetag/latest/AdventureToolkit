package net.threetag.adventuretoolkit.capability;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.quest.IQuestHolder;
import net.threetag.adventuretoolkit.sequence.ISequenceHolder;

import javax.annotation.Nullable;

@Mod.EventBusSubscriber(modid = AdventureToolkit.MODID)
public class ATCapabilities {

    public static void init() {
        CapabilityManager.INSTANCE.register(IQuestHolder.class, new Capability.IStorage<IQuestHolder>() {
                    @Nullable
                    @Override
                    public INBT writeNBT(Capability<IQuestHolder> capability, IQuestHolder instance, Direction side) {
                        if (instance instanceof INBTSerializable)
                            return ((INBTSerializable) instance).serializeNBT();
                        throw new IllegalArgumentException("Can not serialize an instance that isn't an instance of INBTSerializable");

                    }

                    @Override
                    public void readNBT(Capability<IQuestHolder> capability, IQuestHolder instance, Direction side, INBT nbt) {
                        if (instance instanceof INBTSerializable)
                            ((INBTSerializable) instance).deserializeNBT(nbt);
                        else
                            throw new IllegalArgumentException("Can not serialize to an instance that isn't an instance of INBTSerializable");
                    }
                },
                QuestHolderCapability::new);

        CapabilityManager.INSTANCE.register(ISequenceHolder.class, new Capability.IStorage<ISequenceHolder>() {
                    @Nullable
                    @Override
                    public INBT writeNBT(Capability<ISequenceHolder> capability, ISequenceHolder instance, Direction side) {
                        if (instance instanceof INBTSerializable)
                            return ((INBTSerializable) instance).serializeNBT();
                        throw new IllegalArgumentException("Can not serialize an instance that isn't an instance of INBTSerializable");

                    }

                    @Override
                    public void readNBT(Capability<ISequenceHolder> capability, ISequenceHolder instance, Direction side, INBT nbt) {
                        if (instance instanceof INBTSerializable)
                            ((INBTSerializable) instance).deserializeNBT(nbt);
                        else
                            throw new IllegalArgumentException("Can not serialize to an instance that isn't an instance of INBTSerializable");
                    }
                },
                SequenceHolderCapability::new);
    }

    @SubscribeEvent
    public static void onAttachCapabilities(AttachCapabilitiesEvent<Entity> e) {
        if (e.getObject() instanceof PlayerEntity) {
            if(!e.getObject().getCapability(QuestHolderCapability.QUEST_HOLDER).isPresent()) {
                e.addCapability(new ResourceLocation(AdventureToolkit.MODID, "quest_holder"), new QuestHolderProvider(new QuestHolderCapability()));
            }

            if(!e.getObject().getCapability(SequenceHolderCapability.SEQUENCE_HOLDER).isPresent()) {
                e.addCapability(new ResourceLocation(AdventureToolkit.MODID, "sequence_holder"), new SequenceHolderProvider(new SequenceHolderCapability()));
            }
        }
    }

}
