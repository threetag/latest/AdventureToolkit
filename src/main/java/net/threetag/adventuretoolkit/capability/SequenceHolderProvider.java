package net.threetag.adventuretoolkit.capability;

import net.minecraft.nbt.StringNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.threetag.adventuretoolkit.sequence.ISequenceHolder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class SequenceHolderProvider implements ICapabilitySerializable<StringNBT> {

    public final ISequenceHolder instance;
    public final LazyOptional<ISequenceHolder> lazyOptional;

    public SequenceHolderProvider(ISequenceHolder instance) {
        this.instance = instance;
        this.lazyOptional = LazyOptional.of(() -> instance);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        return cap == SequenceHolderCapability.SEQUENCE_HOLDER ? (LazyOptional<T>) this.lazyOptional : LazyOptional.empty();
    }

    @Override
    public StringNBT serializeNBT() {
        if (this.instance instanceof INBTSerializable)
            return ((INBTSerializable<StringNBT>) this.instance).serializeNBT();
        return StringNBT.valueOf("");
    }

    @Override
    public void deserializeNBT(StringNBT nbt) {
        if (this.instance instanceof INBTSerializable)
            ((INBTSerializable<StringNBT>) this.instance).deserializeNBT(nbt);
    }
}
