package net.threetag.adventuretoolkit.capability;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.StringNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.util.INBTSerializable;
import net.threetag.adventuretoolkit.sequence.AbstractSequence;
import net.threetag.adventuretoolkit.sequence.ISequenceHolder;
import net.threetag.adventuretoolkit.sequence.SequenceManager;

public class SequenceHolderCapability implements ISequenceHolder, INBTSerializable<StringNBT> {

    @CapabilityInject(ISequenceHolder.class)
    public static Capability<ISequenceHolder> SEQUENCE_HOLDER;

    private AbstractSequence sequence;

    @Override
    public AbstractSequence getCurrentSequence() {
        return this.sequence;
    }

    @Override
    public void startSequence(AbstractSequence sequence, ServerPlayerEntity player) {
        if (this.sequence != null) {
            this.sequence.end(player);
        }
        this.sequence = sequence;
        this.sequence.start(player);
    }

    @Override
    public void stopCurrentSequence(ServerPlayerEntity player) {
        if (this.sequence != null) {
            this.sequence.end(player);
            this.sequence = null;
        }
    }

    @Override
    public StringNBT serializeNBT() {
        return StringNBT.valueOf(this.sequence != null ? this.sequence.getId().toString() : "");
    }

    @Override
    public void deserializeNBT(StringNBT nbt) {
        this.sequence = SequenceManager.getInstance().getSequence(new ResourceLocation(nbt.getAsString()));
    }
}
