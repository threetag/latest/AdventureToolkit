package net.threetag.adventuretoolkit.capability;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.threetag.adventuretoolkit.quest.IQuestHolder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class QuestHolderProvider implements ICapabilitySerializable<CompoundNBT> {

    public final IQuestHolder instance;
    public final LazyOptional<IQuestHolder> lazyOptional;

    public QuestHolderProvider(IQuestHolder instance) {
        this.instance = instance;
        this.lazyOptional = LazyOptional.of(() -> instance);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        return cap == QuestHolderCapability.QUEST_HOLDER ? (LazyOptional<T>) this.lazyOptional : LazyOptional.empty();
    }

    @Override
    public CompoundNBT serializeNBT() {
        if (this.instance instanceof INBTSerializable)
            return ((INBTSerializable<CompoundNBT>) this.instance).serializeNBT();
        return new CompoundNBT();
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        if (this.instance instanceof INBTSerializable)
            ((INBTSerializable<CompoundNBT>) this.instance).deserializeNBT(nbt);
    }
}
