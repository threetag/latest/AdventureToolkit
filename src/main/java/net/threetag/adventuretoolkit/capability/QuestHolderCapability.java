package net.threetag.adventuretoolkit.capability;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.fml.network.NetworkDirection;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.action.IAction;
import net.threetag.adventuretoolkit.network.NewQuestMessage;
import net.threetag.adventuretoolkit.network.SyncPlayerQuestsMessage;
import net.threetag.adventuretoolkit.network.SyncQuestProgressMessage;
import net.threetag.adventuretoolkit.quest.IQuestHolder;
import net.threetag.adventuretoolkit.quest.Quest;
import net.threetag.adventuretoolkit.quest.QuestInstance;
import net.threetag.adventuretoolkit.quest.QuestManager;
import net.threetag.adventuretoolkit.quest.requirement.progress.AbstractQuestRequirementProgress;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QuestHolderCapability implements IQuestHolder, INBTSerializable<CompoundNBT> {

    @CapabilityInject(IQuestHolder.class)
    public static Capability<IQuestHolder> QUEST_HOLDER;

    private final List<QuestInstance> active = new ArrayList<>();
    private final List<Quest> completed = new ArrayList<>();

    @Override
    public void tick(PlayerEntity player) {
        if (!player.level.isClientSide && player instanceof ServerPlayerEntity) {
            List<QuestInstance> toRemove = new ArrayList<>();
            for (QuestInstance instance : this.active) {
                boolean change = false;
                for (Map.Entry<String, AbstractQuestRequirementProgress> entry : instance.getProgresses().entrySet()) {
                    if (entry.getValue().isDirty()) {
                        entry.getValue().markDirty(false);
                        AdventureToolkit.NETWORK_CHANNEL.sendTo(new SyncQuestProgressMessage(instance.getQuest().getId(), entry.getKey(), entry.getValue().serializeNBT()), ((ServerPlayerEntity) player).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
                        change = true;
                    }
                }

                if (change && instance.getTotalProgress() >= 1F) {
                    for (IAction reward : instance.getQuest().getRewards()) {
                        reward.accept(player);
                    }
                    this.completed.add(instance.getQuest());
                    toRemove.add(instance);
                }
            }

            if (!toRemove.isEmpty()) {
                for (QuestInstance instance : toRemove) {
                    this.active.remove(instance);
                }
                AdventureToolkit.NETWORK_CHANNEL.sendTo(new SyncPlayerQuestsMessage(this.serializeNBT()), ((ServerPlayerEntity) player).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
            }
        }
    }

    @Override
    public boolean startQuest(Quest quest, PlayerEntity player) {
        if (this.active.stream().anyMatch(instance -> instance.getQuest().getId().equals(quest.getId()))) {
            return false;
        }
        this.active.add(new QuestInstance(quest));
        AdventureToolkit.NETWORK_CHANNEL.sendTo(new SyncPlayerQuestsMessage(this.serializeNBT()), ((ServerPlayerEntity) player).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
        AdventureToolkit.NETWORK_CHANNEL.sendTo(new NewQuestMessage(quest.getTitle(), quest.getType()), ((ServerPlayerEntity) player).connection.connection, NetworkDirection.PLAY_TO_CLIENT);
        return true;
    }

    @Override
    public List<QuestInstance> getActiveQuests() {
        return this.active;
    }

    @Override
    public List<Quest> getCompletedQuests() {
        return this.completed;
    }

    @Override
    public void clearQuests() {
        this.active.clear();
    }

    @Override
    public void reload() {
        this.active.forEach(QuestInstance::reload);
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT nbt = new CompoundNBT();

        ListNBT activeNbt = new ListNBT();
        this.active.forEach(instance -> {
            activeNbt.add(instance.serializeNBT());
        });
        nbt.put("ActiveQuests", activeNbt);

        ListNBT completedNbt = new ListNBT();
        this.completed.forEach(instance -> {
            completedNbt.add(StringNBT.valueOf(instance.getId().toString()));
        });
        nbt.put("CompletedQuests", completedNbt);

        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.active.clear();
        ListNBT activeNbt = nbt.getList("ActiveQuests", Constants.NBT.TAG_COMPOUND);
        for (int i = 0; i < activeNbt.size(); i++) {
            QuestInstance instance = new QuestInstance(activeNbt.getCompound(i));
            if (!instance.isObsolete()) {
                this.active.add(instance);
            }
        }

        this.completed.clear();
        ListNBT completedNbt = nbt.getList("CompletedQuests", Constants.NBT.TAG_COMPOUND);
        for (int i = 0; i < completedNbt.size(); i++) {
            Quest quest = QuestManager.getInstance().getQuestById(new ResourceLocation(completedNbt.getString(i)));
            if (quest != null) {
                this.completed.add(quest);
            }
        }
    }
}
