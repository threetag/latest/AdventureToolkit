package net.threetag.adventuretoolkit.sequence;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.action.Actions;
import net.threetag.adventuretoolkit.action.IAction;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class SequenceTypes {

    private static final Map<ResourceLocation, BiFunction<JsonObject, ResourceLocation, AbstractSequence>> SEQUENCE_TYPES = new HashMap<>();

    static {
        registerSequenceType(new ResourceLocation(AdventureToolkit.MODID, "dialogue"), DialogueSequence::new);
    }

    public static void registerSequenceType(ResourceLocation id, BiFunction<JsonObject, ResourceLocation, AbstractSequence> function) {
        SEQUENCE_TYPES.put(id, function);
    }

    public static AbstractSequence parse(JsonObject json, ResourceLocation id) {
        ResourceLocation typeId = new ResourceLocation(JSONUtils.getAsString(json, "type"));

        if (!SEQUENCE_TYPES.containsKey(typeId)) {
            throw new JsonParseException("No such sequence type: " + typeId.toString());
        }

        AbstractSequence sequence = SEQUENCE_TYPES.get(typeId).apply(json, id);

        if (JSONUtils.isValidNode(json, "continuing_actions")) {
            JsonArray array = JSONUtils.getAsJsonArray(json, "continuing_actions");
            for (int i = 0; i < array.size(); i++) {
                JsonObject jsonObject = array.get(i).getAsJsonObject();
                IAction reward = Actions.parse(jsonObject);

                if (reward != null) {
                    sequence.addContinuingAction(reward);
                }
            }
        }

        return sequence;
    }

}
