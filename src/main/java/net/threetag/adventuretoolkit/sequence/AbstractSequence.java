package net.threetag.adventuretoolkit.sequence;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.threetag.adventuretoolkit.action.IAction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractSequence {

    private final ResourceLocation id;
    private final List<IAction> actions = new ArrayList<>();

    public AbstractSequence(ResourceLocation id) {
        this.id = id;
    }

    public ResourceLocation getId() {
        return this.id;
    }

    public Collection<IAction> getContinuingActions() {
        return this.actions;
    }

    public AbstractSequence addContinuingAction(IAction action) {
        this.actions.add(action);
        return this;
    }

    public void start(ServerPlayerEntity player) {

    }

    public void end(ServerPlayerEntity player) {
        for (IAction action : this.getContinuingActions()) {
            action.accept(player);
        }
    }

}
