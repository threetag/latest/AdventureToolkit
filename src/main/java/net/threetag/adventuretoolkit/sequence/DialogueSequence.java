package net.threetag.adventuretoolkit.sequence;

import com.google.gson.JsonObject;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.network.NetworkDirection;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.network.StartDialogueMessage;
import net.threetag.threecore.util.icon.IIcon;
import net.threetag.threecore.util.icon.IconSerializer;

public class DialogueSequence extends AbstractSequence {

    private final IIcon backgroundImage;
    private final IIcon foregroundImage;
    private final ITextComponent name;
    private final ITextComponent text;

    public DialogueSequence(ResourceLocation id, IIcon backgroundImage, IIcon foregroundImage, ITextComponent name, ITextComponent text) {
        super(id);
        this.backgroundImage = backgroundImage;
        this.foregroundImage = foregroundImage;
        this.name = name;
        this.text = text;
    }

    public DialogueSequence(JsonObject json, ResourceLocation id) {
        super(id);

        if (JSONUtils.isValidNode(json, "background")) {
            this.backgroundImage = IconSerializer.deserialize(JSONUtils.getAsJsonObject(json, "background"));
        } else {
            this.backgroundImage = null;
        }

        if (JSONUtils.isValidNode(json, "foreground")) {
            this.foregroundImage = IconSerializer.deserialize(JSONUtils.getAsJsonObject(json, "foreground"));
        } else {
            this.foregroundImage = null;
        }

        this.name = ITextComponent.Serializer.fromJson(JSONUtils.getAsJsonObject(json, "name").toString());
        this.text = ITextComponent.Serializer.fromJson(JSONUtils.getAsJsonObject(json, "text").toString());
    }

    @Override
    public void start(ServerPlayerEntity player) {
        super.start(player);
        AdventureToolkit.NETWORK_CHANNEL.sendTo(new StartDialogueMessage(this.name, this.text, this.backgroundImage, this.foregroundImage), player.connection.connection, NetworkDirection.PLAY_TO_CLIENT);
    }
}
