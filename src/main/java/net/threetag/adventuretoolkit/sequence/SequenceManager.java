package net.threetag.adventuretoolkit.sequence;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import net.minecraft.client.resources.JsonReloadListener;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.threetag.adventuretoolkit.AdventureToolkit;

import java.util.Map;

public class SequenceManager extends JsonReloadListener {

    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    public static SequenceManager INSTANCE;
    private final Map<ResourceLocation, AbstractSequence> registeredSequences = Maps.newHashMap();

    public SequenceManager() {
        super(GSON, "sequences");
        INSTANCE = this;
    }

    @Override
    protected void apply(Map<ResourceLocation, JsonElement> objectIn, IResourceManager resourceManagerIn, IProfiler profilerIn) {
        this.registeredSequences.clear();
        for (Map.Entry<ResourceLocation, JsonElement> entry : objectIn.entrySet()) {
            ResourceLocation resourcelocation = entry.getKey();
            try {
                AbstractSequence sequence = SequenceTypes.parse(entry.getValue().getAsJsonObject(), entry.getKey());
                this.registeredSequences.put(entry.getKey(), sequence);
            } catch (Exception e) {
                AdventureToolkit.LOGGER.error("Parsing error loading sequence {}", resourcelocation, e);
            }
        }
        AdventureToolkit.LOGGER.info("Loaded {} sequence", this.registeredSequences.size());
    }

    public static SequenceManager getInstance() {
        return INSTANCE;
    }

    public AbstractSequence getSequence(ResourceLocation id) {
        return this.registeredSequences.get(id);
    }
}
