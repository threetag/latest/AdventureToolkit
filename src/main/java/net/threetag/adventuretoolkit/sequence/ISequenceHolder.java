package net.threetag.adventuretoolkit.sequence;

import net.minecraft.entity.player.ServerPlayerEntity;

public interface ISequenceHolder {

    AbstractSequence getCurrentSequence();

    void startSequence(AbstractSequence sequence, ServerPlayerEntity player);

    void stopCurrentSequence(ServerPlayerEntity player);

}
