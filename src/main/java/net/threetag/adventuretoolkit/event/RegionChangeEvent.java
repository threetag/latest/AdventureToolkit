package net.threetag.adventuretoolkit.event;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.threetag.adventuretoolkit.region.Region;

public class RegionChangeEvent extends PlayerEvent {

    private final Region previousRegion;
    private final Region newRegion;

    public RegionChangeEvent(PlayerEntity player, Region previousRegion, Region newRegion) {
        super(player);
        this.previousRegion = previousRegion;
        this.newRegion = newRegion;
    }

    public Region getPreviousRegion() {
        return previousRegion;
    }

    public Region getNewRegion() {
        return newRegion;
    }
}
