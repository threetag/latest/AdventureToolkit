package net.threetag.adventuretoolkit.network;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import net.threetag.adventuretoolkit.region.Region;
import net.threetag.adventuretoolkit.region.RegionManager;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class SyncRegionsMessage {

    private final Map<ResourceLocation, Region> regions;

    public SyncRegionsMessage(Map<ResourceLocation, Region> regions) {
        this.regions = regions;
    }

    public SyncRegionsMessage(PacketBuffer buffer) {
        this.regions = new HashMap<>();
        int amount = buffer.readInt();

        for (int i = 0; i < amount; i++) {
            this.regions.put(buffer.readResourceLocation(), Region.read(buffer));
        }
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeInt(this.regions.size());
        this.regions.forEach((id, region) -> {
            buffer.writeResourceLocation(id);
            region.write(buffer);
        });
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
                RegionManager.Client.REGIONS.clear();
                RegionManager.Client.REGIONS.putAll(this.regions);
                RegionManager.generateChildren(RegionManager.Client.REGIONS);
            });
        });
        ctx.get().setPacketHandled(true);
    }
}
