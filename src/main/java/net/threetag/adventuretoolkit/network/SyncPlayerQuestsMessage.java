package net.threetag.adventuretoolkit.network;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import net.threetag.adventuretoolkit.capability.QuestHolderCapability;

import java.util.function.Supplier;

public class SyncPlayerQuestsMessage {

    private final CompoundNBT data;

    public SyncPlayerQuestsMessage(CompoundNBT data) {
        this.data = data;
    }

    public SyncPlayerQuestsMessage(PacketBuffer buf) {
        this.data = buf.readNbt();
    }

    public void toBytes(PacketBuffer buf) {
        buf.writeNbt(this.data);
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
                Minecraft.getInstance().player.getCapability(QuestHolderCapability.QUEST_HOLDER).ifPresent(questHolder -> {
                    if (questHolder instanceof INBTSerializable<?>) {
                        ((INBTSerializable<CompoundNBT>) questHolder).deserializeNBT(this.data);
                    }
                });
            });
        });
        ctx.get().setPacketHandled(true);
    }
}
