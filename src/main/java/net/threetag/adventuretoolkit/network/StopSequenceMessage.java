package net.threetag.adventuretoolkit.network;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.threetag.adventuretoolkit.capability.SequenceHolderCapability;

import java.util.function.Supplier;

public class StopSequenceMessage {

    public StopSequenceMessage() {
    }

    public StopSequenceMessage(PacketBuffer buf) {

    }

    public void toBytes(PacketBuffer buf) {

    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            ctx.get().getSender().getCapability(SequenceHolderCapability.SEQUENCE_HOLDER).ifPresent(sequenceHolder -> {
                sequenceHolder.stopCurrentSequence(ctx.get().getSender());
            });
        });
        ctx.get().setPacketHandled(true);
    }
}
