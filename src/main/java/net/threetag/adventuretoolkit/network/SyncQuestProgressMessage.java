package net.threetag.adventuretoolkit.network;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import net.threetag.adventuretoolkit.capability.QuestHolderCapability;
import net.threetag.adventuretoolkit.quest.QuestInstance;
import net.threetag.adventuretoolkit.quest.requirement.progress.AbstractQuestRequirementProgress;

import java.util.Map;
import java.util.function.Supplier;

public class SyncQuestProgressMessage {

    private final ResourceLocation questId;
    private final String requirementId;
    private final CompoundNBT data;

    public SyncQuestProgressMessage(ResourceLocation questId, String requirementId, CompoundNBT data) {
        this.questId = questId;
        this.requirementId = requirementId;
        this.data = data;
    }

    public SyncQuestProgressMessage(PacketBuffer buffer) {
        this.questId = buffer.readResourceLocation();
        this.requirementId = buffer.readUtf(32767);
        this.data = buffer.readNbt();
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeResourceLocation(this.questId);
        buffer.writeUtf(this.requirementId);
        buffer.writeNbt(this.data);
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
                Minecraft.getInstance().player.getCapability(QuestHolderCapability.QUEST_HOLDER).ifPresent(questHolder -> {
                    for (QuestInstance instance : questHolder.getActiveQuests()) {
                        if (instance.getQuest().getId().equals(this.questId)) {
                            for (Map.Entry<String, AbstractQuestRequirementProgress> entry : instance.getProgresses().entrySet()) {
                                if (entry.getKey().equals(this.requirementId)) {
                                    entry.getValue().deserializeNBT(this.data);
                                    return;
                                }
                            }
                        }
                    }
                });
            });
        });
        ctx.get().setPacketHandled(true);
    }
}
