package net.threetag.adventuretoolkit.network;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.adventuretoolkit.client.renderer.TeleportTransitionRenderer;

import java.util.function.Supplier;

public class StartTeleportTransitionMessage {

    private int duration;
    private SoundEvent soundEvent;

    public StartTeleportTransitionMessage(int duration, SoundEvent soundEvent) {
        this.duration = duration;
        this.soundEvent = soundEvent;
    }

    public StartTeleportTransitionMessage(PacketBuffer buffer) {
        this.duration = buffer.readInt();
        this.soundEvent = buffer.readRegistryIdUnsafe(ForgeRegistries.SOUND_EVENTS);
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeInt(this.duration);
        buffer.writeRegistryIdUnsafe(ForgeRegistries.SOUND_EVENTS, this.soundEvent);
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
                TeleportTransitionRenderer.start(this.soundEvent, this.duration);
            });
        });
        ctx.get().setPacketHandled(true);
    }

}
