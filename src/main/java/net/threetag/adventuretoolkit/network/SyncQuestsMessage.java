package net.threetag.adventuretoolkit.network;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import net.threetag.adventuretoolkit.quest.Quest;
import net.threetag.adventuretoolkit.quest.QuestManager;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class SyncQuestsMessage {

    private final Map<ResourceLocation, Quest> quests;

    public SyncQuestsMessage(Map<ResourceLocation, Quest> quests) {
        this.quests = quests;
    }

    public SyncQuestsMessage(PacketBuffer buffer) {
        this.quests = new HashMap<>();
        int amount = buffer.readInt();

        for (int i = 0; i < amount; i++) {
            ResourceLocation id = buffer.readResourceLocation();
            this.quests.put(id, new Quest(id, buffer));
        }
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeInt(this.quests.size());
        this.quests.forEach((id, quest) -> {
            buffer.writeResourceLocation(id);
            quest.write(buffer);
        });
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
                QuestManager.Client.QUESTS.clear();
                QuestManager.Client.QUESTS.putAll(this.quests);
            });
        });
        ctx.get().setPacketHandled(true);
    }
}
