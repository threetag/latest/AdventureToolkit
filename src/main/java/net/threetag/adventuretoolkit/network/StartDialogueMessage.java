package net.threetag.adventuretoolkit.network;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import net.threetag.adventuretoolkit.client.screen.DialogueScreen;
import net.threetag.threecore.util.icon.IIcon;
import net.threetag.threecore.util.icon.IconSerializer;

import java.util.function.Supplier;

public class StartDialogueMessage {

    private final ITextComponent name;
    private final ITextComponent text;
    private final IIcon background;
    private final IIcon foreground;

    public StartDialogueMessage(ITextComponent name, ITextComponent text, IIcon background, IIcon foreground) {
        this.name = name;
        this.text = text;
        this.background = background;
        this.foreground = foreground;
    }

    public StartDialogueMessage(PacketBuffer buffer) {
        this.name = buffer.readComponent();
        this.text = buffer.readComponent();
        if (buffer.readBoolean()) {
            this.background = IconSerializer.deserialize(buffer.readNbt());
        } else {
            this.background = null;
        }
        if (buffer.readBoolean()) {
            this.foreground = IconSerializer.deserialize(buffer.readNbt());
        } else {
            this.foreground = null;
        }
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeComponent(this.name);
        buffer.writeComponent(this.text);
        buffer.writeBoolean(this.background != null);
        if (this.background != null) {
            buffer.writeNbt(this.background.getSerializer().serializeExt(this.background));
        }
        buffer.writeBoolean(this.foreground != null);
        if (this.foreground != null) {
            buffer.writeNbt(this.foreground.getSerializer().serializeExt(this.foreground));
        }
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            DistExecutor.runWhenOn(Dist.CLIENT, () -> this::openScreen);
        });
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    private void openScreen() {
        Minecraft.getInstance().setScreen(new DialogueScreen(this.name, this.text, this.background, this.foreground));
    }

}
