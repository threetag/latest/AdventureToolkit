package net.threetag.adventuretoolkit.network;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;
import net.threetag.adventuretoolkit.client.gui.toasts.NewQuestToast;
import net.threetag.adventuretoolkit.quest.QuestType;

import java.util.function.Supplier;

public class NewQuestMessage {

    private final ITextComponent title;
    private final QuestType questType;

    public NewQuestMessage(ITextComponent title, QuestType questType) {
        this.title = title;
        this.questType = questType;
    }

    public NewQuestMessage(PacketBuffer buffer) {
        this.title = buffer.readComponent();
        this.questType = QuestType.byName(buffer.readUtf());
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeComponent(this.title);
        buffer.writeUtf(this.questType.getName());
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
                if (this.questType != null) {
                    addToast();
                }
            });
        });
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    private void addToast() {
        Minecraft.getInstance().getToasts().addToast(new NewQuestToast(this.title, this.questType));
    }

}
