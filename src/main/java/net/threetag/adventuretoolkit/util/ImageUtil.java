package net.threetag.adventuretoolkit.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.NativeImage;
import net.minecraft.util.ResourceLocation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Swirtzly
 * on 22/03/2020 @ 15:24
 */
public class ImageUtil {

    public static ResourceLocation createTexture(File file) {
        NativeImage nativeImage = null;
        try {
            nativeImage = NativeImage.read(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Minecraft.getInstance().getTextureManager().register("skin_" + System.currentTimeMillis(), new DynamicTexture(nativeImage));
    }

    public static ResourceLocation createTexture(URL file) throws IOException {
        URLConnection urlConnection = file.openConnection();
        urlConnection.connect();
        urlConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36");
        urlConnection.setReadTimeout(5000);
        urlConnection.setConnectTimeout(5000);
        urlConnection.setUseCaches(false);
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);

        NativeImage nativeImage = null;
        try {
            nativeImage = NativeImage.read(file.openStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Minecraft.getInstance().getTextureManager().register("skin_" + System.currentTimeMillis(), new DynamicTexture(nativeImage));
    }
}
