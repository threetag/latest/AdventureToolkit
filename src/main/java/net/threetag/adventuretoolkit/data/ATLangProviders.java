package net.threetag.adventuretoolkit.data;

import net.minecraft.data.DataGenerator;
import net.threetag.adventuretoolkit.AdventureToolkit;
import net.threetag.adventuretoolkit.block.ATBlocks;
import net.threetag.threecore.data.ExtendedLanguageProvider;

public abstract class ATLangProviders extends ExtendedLanguageProvider {

    /*

    IMPORTANT: Some special characters need an UTF-8 code in here. You can find a table for the codes here: https://www.utf8-chartable.de/
    This mainly applies to norwegian and german, english doesnt really have any.

    Here you have some common ones:
    ø = \u00f8
    å = \u00e5
    Ü = \u00dc
    ü = \u00fc
    Ä = \u00c4
    ä = \u00e4
    Ö = \u00d6
    ö = \u00f6
    ß = \u00df
     */

    public ATLangProviders(DataGenerator gen, String locale) {
        super(gen, AdventureToolkit.MODID, locale);
    }

    public static class English extends ATLangProviders {

        public English(DataGenerator gen) {
            super(gen, "en_us");
        }

        @Override
        protected void addTranslations() {
            this.addBlock(ATBlocks.LIGHT_SOURCE_BLOCK, "Light Source Block");

            this.add("gui.quests.active", "Active Quests");
            this.add("quest.requirement.adventuretoolkit.break_blocks.title", "Break %s %ss");
            this.add("quest.requirement.adventuretoolkit.kill_entities.title", "Kill %s %ss");

            this.add("commands.money.balance.self", "Your balance: %s");
            this.add("commands.money.balance.other", "%s's balance: %s");
            this.add("commands.money.set", "Set %s's balance to %s");
            this.add("commands.money.set.failure", "Was not able to change %s's balance!");
        }

    }

    public static class Norwegian extends ATLangProviders {

        public Norwegian(DataGenerator gen) {
            super(gen, "no_no");
        }

        @Override
        protected void addTranslations() {
            this.addBlock(ATBlocks.LIGHT_SOURCE_BLOCK, "Lys Blokk");

            // quest stuff
        }

    }

    public static class German extends ATLangProviders {

        public German(DataGenerator gen) {
            super(gen, "de_de");
        }

        @Override
        protected void addTranslations() {
            this.addBlock(ATBlocks.LIGHT_SOURCE_BLOCK, "Lichtblock");

            this.add("gui.quests.active", "Aktive Auftr\u00e4ge");
            this.add("quest.requirement.adventuretoolkit.break_blocks.title", "Baue %s %ss ab");
            this.add("quest.requirement.adventuretoolkit.kill_entities.title", "T\u00f6te %s %ss");

            this.add("commands.money.balance.self", "Kontostand: %s");
            this.add("commands.money.balance.other", "%ss Kontostand: %s");
            this.add("commands.money.set", "%ss Kontostand auf %s gesetzt");
            this.add("commands.money.set.failure", "Fehler beim \u00c4ndern von %ss Kontostand!");
        }

    }

    @Override
    public String getName() {
        return "AdventureToolkit " + super.getName();
    }
}
