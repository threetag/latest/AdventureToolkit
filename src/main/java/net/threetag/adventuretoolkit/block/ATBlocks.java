package net.threetag.adventuretoolkit.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ObjectHolder;
import net.threetag.adventuretoolkit.AdventureToolkit;

import java.util.Objects;

public class ATBlocks {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, AdventureToolkit.MODID);

    public static final RegistryObject<Block> LIGHT_SOURCE_BLOCK = BLOCKS.register("light_source_block", () -> new LightSourceBlock(AbstractBlock.Properties.of(Material.BARRIER).strength(-1.0F, 3600000.8F).noDrops().noOcclusion().isValidSpawn((state, reader, blockPos, a) -> false).lightLevel((blockState) -> blockState.getValue(LightSourceBlock.LIGHT))));

    @ObjectHolder(AdventureToolkit.MODID)
    @Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class Items {

        @SubscribeEvent
        public static void onRegisterItems(RegistryEvent.Register<Item> e) {
            e.getRegistry().register(new BlockItem(LIGHT_SOURCE_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_DECORATIONS)).setRegistryName(Objects.requireNonNull(LIGHT_SOURCE_BLOCK.get().getRegistryName())));
        }

    }

}
